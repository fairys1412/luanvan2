﻿A new natural jasmonoid glucoside isolated from Euphorbia hirta L. extract
DOI: 10.22144/ctu.jen.2020.014
Le Thi Bach1,2, Le Tien Dung3, Nguyen Trong Tuan1 and Bui Thi Buu Hue1*
1Department of Chemistry, College of Natural Sciences, Can Tho University
2Graduate University of Science and Technology, Vietnam Academy of Science and Technology
3Institute of Applied Materials Science, Vietnam Academy of Science and Technology
*Correspondence: Bui Thi Buu Hue (email: btbhue@ctu.edu.vn)
Article info.

ABSTRACT
Received 04 Mar 2020
Revised 25 May 2020
Accepted 31 Jul 2020

Euphorbia hirta L., one of the species belonging to the genus Euphorbia, Euphorbiaceae family, is common in the Mekong Delta of Vietnam. The present study was designed to investigate the phytochemicals of Euphorbia hirta L. collected in Can Tho city. Repeated chromatography of the methanol fraction on silica gel columns afforded a jasmonoid glucoside sodium salt, a new compound, while four known compounds were isolated from ethyl acetate extract. Their structures were elucidated by analysis of spectral data and in comparison with the published literature data.
Keywords


Chemical components, Euphorbia hirta L., jasmonoid glucoside


Cited as: Bach, L.T., Dung, L.T., Tuan, N.T. and Hue, B.T.B., 2020. A new natural jasmonoid glucoside isolated from Euphorbia hirta L. extract. Can Tho University Journal of Science. 12(2): 40-44.

1.1INTRODUCTION
Euphorbia hirta L., belonging to genus Euphorbia, Euphorbiaceae family, is frequently seen to occupy open waste spaces and grasslands, road side and pathways in many parts of the world. It has been widely used as a traditional medicinal herb in many tropical countries. The leaves of E. hirta L. are found to contain flavonoids, polyphenols, tannins, sterols, alkaloids, glycosides, and triterpenoids (Kumar et al., 2010). 
There were several researches on pharmaceutical application of E. hirta L. in the world. The whole plant was commonly applied to cure various diseases, especially gastrointestinal disorders, affections of the skin and mucous membranes, and respiratory system (Johnson et al., 1999; Kumar et al., 2010). Recently, pharmacological investigations have shown that E. hirta L. and its active components possessed a wide range of bioactivities such as anti-inflammatory, antifungal, antibacterial, antidiarrheal, antioxidant activities (Essiett and Okoko 2013; Hore et al., 2006; Youssouf  et al., 2007). Furthermore, there are only a few reports on the chemical compositions and the biological activities of this species from Vietnam. Therefore, this paper announced the separation and characterization of a novel jasmonoid glucoside, along with four known compounds from E. hirta L.
1.2METHODOLOGY
1.3Plant material
The whole plant was collected at the end of February 2016 in Can Tho city, Vietnam. The plant sample was authenticated by Dr. Dang Minh Quan, Department of Biology Education, Can Tho University where a voucher specimen was deposited. The raw materials were left to dry in the shade at room temperature for some days until being well-dried.
1.4General procedures
Nuclear Magnetic Resonance (NMR) spectra were recorded on a Bruker AM500 FTNMR spectrometer (Bruker, Karlsruhe, Germany) using TMS as an internal standard, Institute of Chemistry - Vietnam Academy of Science and Technology. High Resolution Electrospray Ionization Mass Spectrum (HR-ESI-MS) was also performed at Institute of Chemistry - Vietnam Academy of Science and Technology. Thin Layer Chromatography (TLC) was performed on silica gel 60 F254 (0.063–0.200 mm, Merck, Germany) and RP-18 F254 plates (Merck, Germany). The detection of compounds on TLC plates was done using UV lamp at 254 or 365 nm or a solution of FeCl3/EtOH or H2SO4/EtOH. Column chromatography was performed on silica gel (240-430 mesh, Merck, Germany) and ODS (70-230 mesh, Merck, Germany). 
1.5Extraction and isolation
 The well-dried plant was ground into powder (8 kg) which was then soaked in 96% ethanol at room temperature for five times (5×20 L) and filtered. The filtrate was concentrated under reduced pressure to give brown residue as crude ethanol extract (CE, 700 g). This crude extract was then fractionated on flash column chromatography successively with n-hexane, ethyl acetate, and methanol, respectively to yield the corresponding of n-hexane (HE, 160 g), ethyl acetate (EE, 95 g), and methanol (ME, 172 g) extracts. 
The methanol extract was subjected to flash column chromatography on silica gel and eluted with various proportions of ethyl acetate and methanol (50:1-0:100) to obtain seven fractions (ME 1-7). The fraction ME4 was subjected to a silica gel column chromatography (CC) and eluted with EtOAc: MeOH (40:1-0:100) to obtain 16 fractions (ME 4.1-16). Fraction ME 4.13 was further separated on a silica gel CC and eluted with CHCl3: MeOH (5:1-0:100) to yield seven subfractions (ME 4.13.1-7). Subfraction ME 4.13.3 was further chromatographed on silica gel CC Rp18, eluted with MeOH: H2O (0:100-100:0) to afford subfraction (ME 4.13.3.6) which was then re-chromatographed on silica gel CC Rp18 using MeOH: H2O (1:5) as eluent to obtain compound 1 (6 mg).
The ethyl acetate extract was also subjected to flash column chromatography on silica gel CC and eluted with gradient of n-hexane and ethyl acetate (100:0- 0:100) to obtain eight fractions (EE 1-8). Fraction EE 7 was further separated on a silica gel column eluted with gradient of n-hexane: EtOAc (1:1-0:100). Subfraction EE 7.10 was continually chromatographed on silica gel CC, eluted with CHCl3: MeOH (40:1-10:1). Subfractions EE 7.10.6 was further chromatographed on silica gel CC Rp18 with MeOH: H2O (1:1) as eluent and compound 2 (5 mg) was obtained.
Subfraction EE 7.11 was subjected to silica gel CC and eluted with CHCl3: MeOH (30:1-5:1) to give seven subfractions (EE 7.11.1-7). Using a silica gel CC Rp18 column with MeOH:  H2O as eluent for subfraction EE 7.11.7 to give compound 3 (16 mg). 
Subfraction EE 7.7 was further chromatographed on silica gel CC, eluted with CHCl3: MeOH (10:1-5:1) and compound 4 (11 mg) was obtained.
Subfraction EE 7.8 was fractionated by a column chromatography on silica gel using a mixture of CHCl3: MeOH (20:1-5:1) to yield four subfractions (EE 7.8.1-4). Subfraction EE 7.8.4 was rechromatographed on a silica gel column eluting with CHCl3: MeOH (3:1) to obtain compound 5 (7 mg). 
1.6RESULTS AND DISCUSSION
Compound 1 was obtained from methanol extract as white powder. 
The 1H-NMR (CD3OD, 500 MHz) showed the signals of a double bond with Z configuration at H [5.53 (1H, td, J = 11.5 and 6.0 Hz)] and H [5.47 (1H, td, J = 11.5 and 4.5 Hz)], an oxymethylene group at [H 3.92 (1H, m); 3.71 (1H, dd; J = 12.0 and 5.5 Hz)], four methylene groups at H [2.63 (dd; J = 14.0 and 4.5 Hz); 2.22 (dd; J = 14.0 and 9.0 Hz)]; [2.10 (dt, J = 8.5 and 2.5 Hz); 2.34 (m)]; 2.46 (m) and 2.41 (m). Additionally, the 1H-NMR gave typical signals of a sugar moiety including anomeric proton at H 4.31, oxymethine protons at H 3.22-3.40 and oxymethylene protons at H [3.88 (m); 3.61 (dd, J = 17.0 and 7.0 Hz)]. 
The 13C-NMR (CD3OD, 125 MHz), DEPT, and Heteronuclear Multiple Bond Correlation (HMBC) spectra of compound 1 displayed 18 carbon signals, including six carbons of glucose, one oxymethylene carbon at C 70.2 (C-12), two olefinic carbons at C [129.3 (C-9); 128.6 (C-10)], five methylene carbons at C [43.4 (C-2); 28.4 (C-4); 38.7 (C-5); 29.1 (C-11); 26.5 (C-8)], two methine carbons at C [40.1 (C-3); C 55.6 (C-7), one carbonyl group at C 222.8 (C-6), and one carboxyl group at C 182.0 (C-1). 
The presence of five-carbon substituent pent-1-ol-3-en-5-yl was confirmed by the correlations from COSY spectrum between H-12 and H-11; H-11 and H-10; H-10 and H-9; H-9 and H-8. In addition, COSY spectrum also indicated the position of double bond at C-9 and C-10 through the cross peaks between H-10 with H-11 and H-9 with and H-8. Furthermore, the HMBC data obtained for these protons correlated with the COSY-derived sequence above (via correlations from H-11 and H-8 to C-9 and C-10) indicating that the position of the double bond must be at C-9 and C-10. The chemical shift of less than 30 ppm of the two methylene carbons assigned the Z-configuration of the double bond. (Kang  et al., 2001). The presence of ethanoic-2-yl (–CH2–COOH) group was also confirmed by the HMBC correlations between methylene proton [H 2.63 (dd; 4.5 Hz and 14.0 Hz); 2.22 (dd; 14.0 Hz and 9.0 Hz), H-2] and carboxyl carbon at C 182.0 (C-1). 
 The correlations between proton H-3 H 2.33 (m) with C-4, C-5; proton H-4 [H 2.28 (m); 1.60 (dt, 12.0 and 5.0 Hz)] with C-3, C-5, C-6 and C-7; proton H-5 [H 2.10 (dt, 8.5 and 2.5 Hz); 2.34 (m)] with C-3, C-4, C-6 and C-7 was observed in HMBC spectrum. In addition, cross peaks between H-3, H-7; H-3, H-4 and H-4, H-5 was also observed in the COSY spectrum. These data evidenced the presence of cyclopentanone moiety in the molecule.
The HMBC correlation between cyclopentanone at H-7 [H 2.00 (dt, 5.5 and 10.0 Hz)] with pent-1-ol-3-en-5-yl at C-8 [C 26.5] indicated the location of this side chain at C-7. Moreover, there was an HMBC cross peak between H-2 and C-3, 4, 7 suggested for the C-3 position of ethanoic-2-yl. The spectra also indicated the existence of β-glucose moiety via the chemical shift values and coupling constant of an anomeric proton (8.0 Hz). The HMBC correlation between anomeric proton with C-12 and between H-12 with anomeric carbon proved that β-glucose attached to C-12 of aglycone through O-glucosidic linkage.
The spectral data indicated that this compound was a derivative of 12- hydroxyjasmonic acid. There was no correlation between proton H-3 and proton H-7 in NOESY spectrum which suggested for trans-isomer. This was reconfirmed by comparison the chemical shift values of C-3 and C-7 with similar compounds reported previously. In addition, the typical proton coupling constant of axial-axial pattern was normally more than 7.0 Hz and that of axial-axial pattern was generally less than 4.0 Hz. In compound 1, coupling constants of H-7 were 5.5 and 10.0 Hz supported that this proton coupled with nearby H-3 through axial-axial pattern which proved H-7 and H-3 were trans-oriented protons. The specific rotation of 1, = - 48.3 (c 0.05, MeOH), was in agreement with the assigned stereochemistry (Dathe  et al., 1981; Fujita  et al., 1996; Husain et al., 1993).
The signal of carboxyl group displayed at δC 182.0 on the 13C-NMR spectrum (methanol-d4), in previous references, the carboxylic group showed at δC (176-177 ppm) at the same solvent. Therefore, the carboxyl group in this compound was assigned as a carboxylate group. In addition, methylene carbon C-2 was shifted to downfield at 43.4 ppm due to the electron-withdrawing of carboxylate group (comparison with 37-39 ppm in case of carboxylic group) (Fujita et a., 1996; Xu et al., 2014).
The HR-ESI-MS of the compound 1 m/z 413.1729 [M+H]+ (calculated 413.1757), gave its molecular formula C18H25NaD2O9. This data was reconfirmed with the existence of sodium carboxylate in the compound and the hydrogen-deuterium exchange with methanol in the solvent MeOD usually occur when we measure MS after measuring NMR.
Based on the spectral evidence, the molecular formula of compound 1 as C18H27NaO9. This is a new compound (SciFinder results on 22/11/2018 at the Université catholique de Louvain, Belgium) and characterized as sodium β-D-glucopyranosyl 12-hydroxyjasmonate.
Compound 2 was characterized as a yellow amorphous solid. 1H-NMR (CD3OD, 500 MHz), H (ppm): 7.78 (2H, d, 8.25 Hz, H-2,6); 6.95 (2H, d, 8.5 Hz, H-3,5); 6.39 (1H, d, 2.0 Hz, H-8); 6.22 (1H, d, 2.0 Hz, H-6); 5.39 (1H, d, 1.5 Hz, H-1; 4.24 (1H, dd, 3.5 Hz, 1.5 Hz, H-2; 3.73 (1H, dd, 9.0 and 3.5 Hz, H-3; 3.34 (2H, m, H-4, 5); 0.94 (3H, d, 5.5 Hz, H-6). 13C-NMR (CD3OD, 125 MHz), C (ppm): 179.6 (C-4); 166.0 (C-7); 163.2 (C-5); 161.6 (C-4); 159.3(C-9); 158.6(C-2), 136.2 (C-3); 131.9 (C-2,6); 122.7 (C-1); 116.5 (C-3,5); 105.9 (C-10); 103.5 (C-1); 99.9 (C-6); 94.8 (C-8); 73.2 (C-5); 72.2 (C-3); 72.0 (C-4); 71.9 (C-2); 17.7 (C-6).
Compound 3 was characterized as a yellow solid. 1H-NMR (CD3OD, 500 MHz), H (ppm): 6.99 (2H, s, H-2, 6); 6.40 (1H, d, 2.0 Hz, H-8); 6.24 (1H, d, 2.0 Hz, H-6); 5.36 (1H, s, H-1); 4.23 (1H, s, H-2); 3.81 (1H, dd, 9.3 and 3.3 Hz, H-3); 3.52 (1H, m, H-5); 3.33 (1H, m, H-4); 1.00 (3H, d, 6.0 Hz, H-6). 13C-NMR (CD3OD, 125 MHz), C (ppm): 179.6 (C-4); 165.8 (C-7); 163.2 (C-5); 159.3 (C-2); 158.4 (C-9); 146.9 (C-3, 5); 136.2 (C-3); 109.6 (C-2, 6); 105.9 (C-10); 103.5 (C-1); 99.8 (C-6); 94.7 (C-8); 73.3 (C-4); 72.1 (C-3); 72.0 (C-5); 71.8 (C-2); 17.8 (C-6).

Table 1: Table of  13C (125 Hz) and 1H-NMR (500 Hz) data of compound 1 and reference compound 
Position
Compound 1
12-β-D-Glucopyranosyloxy jasmonic acid 

δH ppm (J, Hz)
CD3OD, 500 MHz
δC ppm
125 MHz
HMBC (HC)
δC ppm
CD3OD, 125 MHz
1

182
-
176.2
2
2.63 (dd; 14.0 and 4.5); 2.22 (dd; 14.0 and 9.0) 
43.4
C-1, 3, 4
38.6
3
2.33 (m)
40.1
C-4, 5
38.9
4
2.28 (m); 1.60 (dt, 12.0 and 5.0)
28.4
C-3, 5, 6, 7
28.1
5
2.10 (dt, 8.5 and 2.5); 2.33 (m)
38.7
C-3, 4, 6, 7
39.7
6

222.8
-
222.1
7
2.00 (dt, 10.0 and 5.5)
55.6
C-3, 6, 8
55.0
8
2.46 (m)  
26.5
C-3, 6, 7, 9, 10
26.2
9
5.47 (td, 11.5 and 4.5)
129.3
C-8
128.8
10
5.53 (td, 11.5 and 6.0)
128.6
C-8, 9, 11, 12
128.8
11
2.41 (m)
29.1
C-10, 12
28.9
12
3.92 (m); 3.71 (dd; 12.0 and 5.5)
70.2
C-10, 11, 1
70.1
1
4.31 (d; 8.0)
104.3
C-12, 2
104.2
2
3.22 (dd; 9.5 and 8.0)
75.1
C-1, 3
74.9
3
3.40 (t; 9.5)
77.9
C-2, 4
77.7
4
3.30 (m)
71.7
C-3, 5
71.5
5
3.31 (m)
78.1
C-4, 6
77.9
6
3.88 (m); 3.61 (dd, 17.0 and 7.0)
62.8
C-4, 5
62.6


Figure 1: Structure of compounds 1-5

Compound 4 was characterized as white solid. 1H-NMR (CD3OD, 500 MHz), H (ppm): 7.10 (2H, s, H-2, 6). 13C-NMR (CD3OD, 125 MHz), C (ppm): 170.6 (-COOH); 146.4 (C-3, 5); 139.5 (C-4); 122.3 (C-1); 110.3 (C-2, 6).
Compound 5 was characterized as brown solid. 1H-NMR (acetone-d6, 500 MHz), H (ppm): 7.53 (1H, d, 2.0 Hz, H-2); 7.48 (1H, dd, 8.5 and 2.0 Hz, H-6); 6.90 (1H, d, 8.5 Hz, H-5). 13C-NMR (acetone-d6, 125 MHz), C (ppm): 167.6 (C-7); 150.7 (C-4); 145.6 (C-3); 123.6 (C-6); 123.1 (C-1); 117.5 (C-2); 115.7 (C-5). 
By comparing the NMR spectral data with 
those reported in literature, the structure of compounds 2-5 were identified as afzelin (Lee et al., 2014), myricitrin (Phan et al., 2015), gallic acid (Prihantini  et al., 2015) and protocatechuic acid (Da Silva  et al., 2015), respectively (Fig. 1).
1.7CONCLUSIONS
From the extracts of Euphorbia hirta L. grown in Vietnam, sodium β-D-glucopyranosyl 12-hydroxyjasmonate (1), afzelin (2), myricitrin (3), gallic acid (4), and protocatechuic acid (5) were isolated and determined. In which, compound 1 is a new compound. 
ACKNOWLEDGMENT
This research was financially supported by the Project AQUABIOACTIVE, ARES, Belgium. 
REFERENCES
Da Silva, L. A. L., Faqueti, L. G., Reginatto, F. H., dos Santos, A. D. C., Barison, A., and Biavatti, M. W., 2015. Phytochemical analysis of Vernonanthura tweedieana and a validated UPLC-PDA method for the quantification of eriodictyol. Revista Brasileira de Farmacognosia. 25: 375-381.
Dathe, W., Rönsch, H., Preiss, A., Schade, W., Sembdner, G., and Schreiber, K. 1981. Endogenous plant hormones of the broad bean, Vicia faba L. (-)-jasmonic acid, a plant growth inhibitor in pericarp. Planta. 153: 530-535.
Essiett, U., and Okoko, A. 2013. Comparative nutritional and phytochemical screening of the leaves and stems of Acalypha fimbriata Schum. & Thonn. and Euphorbia hirta Linn. Bulletin of Environment, Pharmacology and Life Sciences. 2: 38-44.
Fujita, T., Terato, K., Nakayama, M. 1996. Two jasmonoid glucosides and a phenylvaleric acid glucoside from Perilla frutescens. Bioscience, Biotechnology, and Biochemistry. 60: 732-735.
Hore, S., Ahuja, V., Mehta, G., Kumar, P., Pandey, S., Ahmad, A., 2006. Effect of aqueous Euphorbia hirta leaf extract on gastrointestinal motility. Fitoterapia. 77: 35-38.
Husain, A., Ahmad, A., Agrawal, P. K., 1993. (-)-Jasmonic acid, a phytotoxic substance from Botryodiplodia theobromae: characterization by NMR spectroscopic methods. Journal of Natural Products. 56: 2008-2011.
Johnson, P. B., Abdurahman, E. M., Tiam, E. A., Abdu-Aguye, I., Hussaini, I. M., 1999. Euphorbia hirta leaf extracts increase urine output and electrolytes in rats. Journal of Ethnopharmacology, 65(1): 63-69.
Kang, S. S., Kim, J. S., Son, K. H., Kim, H. P. and Chang, H. W., 2001. Cyclooxygenase-2 inhibitory cerebrosides from Phytolaccae radix. Chemical and Pharmaceutical Bulletin, 49(3): 321-323.
Kumar, S., Malhotra, R., Kumar, D., 2010. Euphorbia hirta: Its chemistry, traditional and medicinal uses, and pharmacological activities. Pharmacognosy Reviews. 4: 58.
Lee, S. Y., So, Y. J., Shin, M. S., Cho, J. Y., and Lee, J. 2014. Antibacterial effects of afzelin isolated from Cornus macrophylla on Pseudomonas aeruginosa, a leading cause of illness in immunocompromised individuals. Molecules. 19: 3173-3180.
Phan, N. H. T., Thuan, N. T. D., Van Duy, N., et al., 2015. Flavonoids isolated from Dipterocarpus obtusifolius. Vietnam Journal of Chemistry. 53(2e): 131-136.
Prihantini, A. I., Tachibana, S., Itoh, K., 2015. Antioxidant active compounds from Elaeocarpus sylvestris and their relationship between structure and activity. Procedia Environmental Sciences. 28: 758-768.
Xu, X., Xie, H., Wei, X., 2014. Jasmonoid glucosides, sesquiterpenes and coumarins from the fruit of Clausena lansium. LWT-Food Science and Technology. 59: 65-69.
Youssouf, M.,Kaiser, P., Tahir, M., et al., 2007. Anti-anaphylactic effect of Euphorbia hirta. Fitoterapia. 78: 535-539.