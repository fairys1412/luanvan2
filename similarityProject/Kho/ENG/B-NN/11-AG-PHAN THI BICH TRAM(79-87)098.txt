﻿
DOI:10.22144/ctu.jsi.2018.098
Optimization of the polyphenolics extraction from red rice bran by response surface methodology
Phan Thi Bich Tram1*, Nguyen Tri Liem2 and Duong Thi Phuong Lien1
1College of Agriculture and Applied Biology, Can Tho University, Vietnam
2High School of Ngo van Can, Ben Tre, Vietnam
*Correspondence: Phan Thi Bich Tram (email: ptbtram@ctu.edu.vn)
ARTICLE INFO

ABSTRACT
Received 23 May 2018
Revised 01 Jul 2018
Accepted 03 Aug 2018

Recently, the interest towards phenol compounds has progressed. In this study, a response surface methodology (RSM) was applied to predict the optimum conditions for extraction of phenolic from red rice bran. Firstly, the effects of ethanol concentration (20–80%), added acetic acid concentration (0–20%), extraction time (0–6.5 hours), temperature (25–100oC), number of extraction cycles (1–4) and solid–solvent ratio (1/4–1/10 w/v) on total polyphenolic content (TPC) of red rice bran were found. After that, three parameters such as solvent concentration (48–68%), added acetic acid concentration (10–15%) and extraction time (120–240 min) were able to be optimized using the Box Behnken design (BBD) with a quadratic regression model built by using RSM. The experiment was designed according to 30 runs with 2 blocks; each block consists of 3 central points and 1 replicate. This design was set up for 3 factors to optimise the response in term of TPC extraction. The extracts were analyzed the TPC spectrophotometrically according to the Folin-Ciocalteu colorimetric assay. The optimal extraction conditions were determined as follows: ethanol concentration of 54.5%, added acetic acid concentration of 13.1%, extraction time of 210 min, temperature of 40oC, 3 cycles of extraction and solid–solvent ratio 1/6 w/v. Using these extraction conditions, the experimental yield of TPC was 2391.1±5.9 mg GAE/100 g dry weight (dw) that was in close significant agreement with predicted value (p < 0.05). The experimental results were fitted to a second order quadratic polynomial model, and they have shown a good fit to the proposed model (R2 = 0.99). With these conditions, the antioxidant capacity assayed by 1,1–diphenyl–2–picrylhydrazyl (DPPH) radical–scavenging activity in term of IC50 value of extract was 108.1±2.9 μg dw/mL. The study result indicates the suitability of the developed model and the success of RSM in optimizing the extraction conditions.
KEYWORDS


Antioxidant activity, extraction, phenolic compounds, red rice bran, response surface methodology


Cited as: Tram, P.T.B., Liem, N.T. and Lien, D.T.P., 2018. Optimization of the polyphenolics extraction from red rice bran by response surface methodology. Can Tho University Journal of Science. 54(Special issue: Agriculture): 79-87.

1.1INTRODUCTION
In many Asian countries, red rice (Oryza sativa L.) was considered to be a traditional staple crop (Chen el al., 2015). Red rice has been considered as a nutritious food for weak people in traditional Chinese medicine (Huang and Lai, 2016) and was also gaining popularity as a functional crop owing to its high polyphenols content (Itani and Ogawa, 2004), anthocyanins (Oki el al., 2002) and other nutrition components (Yoshida el al., 2010). Many authors found that the total phenolic content was four times higher in pigmented rice than that in non-pigmented varieties (Yawadio el al., 2007; de Mira el al., 2009). Red rice varieties showed higher total phenolics, flavonoids and antioxidant activity than those of white and brown varieties (Min el al., 2012; Shao el al., 2014).
The “colored rice” is related to the accumulation of phytochemical compounds that  are pigment containing and generally accumulate in perdicarp, testa or bran of the rice kernel (Lee, 2010; Muntana and Prasong, 2010). For this reason, rice bran is considered a rich source of phytochemicals including phenolic compounds which can be used as free radical scavengers (Chen and Bergman, 2005; Rao el al., 2010). 
For extraction of phenolic compounds in rice bran, the most commonly used solvents are methanol, ethanol and water acidified with acetic acid (Pereira-Caro el al., 2013; Shao el al., 2014). Ethanol and organic acids are expected because they are less toxic than methanol (Escribano-Bailón el al., 2004). Other factors such as concentration and volume of solvent, numbers of extraction cycles, solid–liquid ratio, temperature and time have significant influence on the extraction process. The combination of these factors and the determination of optimal conditions are important in order to obtain a maximum yield of phenolics. From a practical standpoint, research using random single factor has been conducted (Pedro el al., 2016) to extract phenolic compounds from materials. Factors that have significant influence on the yield should be optimized using of mathematical models that describe accurately the isolated and combination effects of different factors. Response surface methodology (RSM) is a powerful mathematical technique based on regression analysis used to develop and optimize products and processes that have two or more factors that influence the response (Baş and Boyacı, 2007; Granato el al., 2010; Bassani el al., 2014). It can be used to determine the best conditions for the extraction of chemical compounds from natural products (Granato el al., 2014a; Granato el al., 2014b). Thus, this study was aimed at extracting and modeling, through RSM, the total phenolic content of red rice and to determine antioxidant activity of red rice bran corresponding to the optimum extraction conditions. 
1.2MATERIALS AND METHODS
1.3Red rice bran
Red rice (Huyet rong) variety was supplied from Vinh Hung district, Long An province, Vietnam. The crude rice bran was collected from Long An Rice Processing Factory. Crude rice bran was blown through the sieve for impurities and shred husks removing. To maintain the antioxidant compounds as well as antioxidant activity, rice bran was dried at 45–50oC to reduce the  moisture content down to 10–12%. The rice bran oil was extracted using petroleum ether by the Soxhlet extraction. Defatted red rice bran samples were kept in polyethylene bag and stored at 0–4oC for one week, i.e. until analyses (Iqbal el al., 2005; Do el al., 2014; Huang and Lai, 2016).   
1.4Experimental design
Single factors of extration process such as ethanol concentration (20–80%), added acetic acid concentration (0–20%), time (0–6.5 hours), temperature (25–100oC), number of extraction cycles (1–4) and solid– solvent ratio (1/4–1/10 w/v) were studied seriatim on total polyphenolic content (TPC) of red rice bran extracts. Three parameters, solvent concentration (48–68%), added acetic acid concentration (10–15%) and extraction time (120–240 min), were able to be optimized using the Box Behnken design (BBD) with a quadratic regression model built by using RSM. The experiment was carried out according to 30 runs 3 independent variables with 2 blocks including of 3 central points and 1 replicate for each to optimise the TPC extraction.
1.5Determination of TPC
The TPC was determined by Folin-Ciocalteu method (Rao el al., 2010). The TPC of samples was expressed as milligrams garlic acid equivalents per 100 gram of dry matter (mg GAE/100 g).
1.6Determination of total anthocyanin content (TAC) 
The TAC was determined by using pH differential method (Sripakdee el al., 2017). A spectrophotometer was used for the spectral measurements at 210 nm and 750 nm. TAC was calculated as follows: TAC (mg CGE/ 100 g) = (A × M × DF × 100)/(ε × 1). 
Where, A= (Absorbance λ vis‐max‐A750) Ph 1.0‐ (Absorbance λ vis‐max‐A750) pH 4.5. M was molecular weight of anthocyanin (cyd‐3‐glu) = 449 (g/mol), Extraction coefficient (ε) = 29,600 L/(mol.cm), DF=Diluted factor.
1.7Determination of antioxidant activity
Antioxidant activity of the phytochemicals extracted from rice bran was assessed by measuring their radical scavenging activity that was measured by the bleaching of the purple-colored methanol solution of 2,2-diphenyl-1-picrylhydrazyl (DPPH). The stable DPPH radical as a reagent was used for this spectrophotometric assay. The DPPH radical scavenging activity was evaluated from the difference in peak area decrease of the DPPH radical detected at 517 nm between a blank and a sample (Rao el al., 2010). Percentage of radical scavenging activity was plotted against the corresponding concentration of the extract (μg/ml)  to obtain IC50 value in mg/ml.
1.8Data analysis
The response variables were fitted to a general form of quadratic polynomial model that was showed in equation (1): 
Y = βo  + ∑βixi  + ∑βiixi2 + ∑βijxixj		(1)
Where, Y represents the predicted responses; βo, βi, βii  and βij  are the coefficients of intercept, linear, squared and interaction; xi and xj are the independent variables. Portable Statgraphics Centurion software was used to estimate the response of each set of experimental design and optimized conditions. The fit of the models was inspected by the regression coefficient R2.
1.9RESULTS AND DISCUSSION
1.10Effect of single factors from extraction process on TPC 
Effect of ethanol concentration on TPC content of the extracts
The relationship between ethanol concentration and TPC in the rice bran extracts was shown in Figure 1 and obeyed the quadratic equation (R2 = 0.99) as follows:
TPC = –0.621 (EtOH)2 + 71.68 (EtOH) – 10.76       (2)
Where, (EtOH) is ethanol concentration (%).
When the ethanol concentration increased from 20% to about 60%, the total phenolic content of the extracts increased. In addition, that the ethanol concentration reached 80% resulted in the decrease in the TPC of the extracts (Figure 1). In general, the polarity of the ethanol-water mixture increased continuously with the addition of water to the ethanol. More polar phenolic compounds may be extracted according to the “like dissolves like” principle (Tabaraki and Nateghi, 2011). In this study, the effective range of ethanol concentration on TPC of the extract was about 48–68%. The estimated maximum value of TPC was 2,061 mg GAE/100 g corresponding to ethanol concentration of 57.8%. A similar result was reported in the extraction of phenolic antioxidants from rice bran and brown rice using ethanol 60% as solvent (Tabaraki and Nateghi, 2011; Chooklin, 2013).


Figure 1: Effect of ethanol concentration on TPC

Effect of acetic acid concentration on TPC of the extract
Many authors reported that the pH variation could have a positive or a negative effect on extraction of flavonoids and polyphenols, depending on the interaction of the polyphenols with other constituents of each plant (Pinelo el al., 2004; Michiels el al., 2012; Boeing el al., 2014). Acetic acid added to ethanol solution increased TPC of rice bran extract. The correlation of two variables was displayed in Figure 2 and followed quadratic equation:
TPC = –0.92 (Ac.A)2 + 22.97 (Ac.A) + 1993	(3)
Where, (Ac.A) is acetic acid concentration (%).
The estimated maximum value of TPC was 2,136.4 mg GAE/100 g corresponding to acetic acid concentration of 12.5%. 


Figure 2: Effect of acetic acid concentration on TPC

Effect of extraction time on TPC of the extracts
Extraction time is one of important factors of extraction process (Pedro el al., 2016). The extraction time affected markedly TPC of rice bran extracts, and the correlation of two variables was shown in Figure 3. The regression (R2=0.99) representing the dependence of TPC on extraction time is shown as:
TPC = –48.48 (τ)2 + 278.5 (τ) + 1748 			(4)
Where, (τ) is extraction time (hours)

Figure 3: Effect of extraction time on TPC

It has been observed that with the prolongation of the extraction time up to 3 hours, the output of polyphenols increased significantly but there was a significant decrease in TPC with the prolongation of extraction time up to 6 hours. These phenomena could be explained by the Fick’s second law of diffusion, predicting that a final equilibrium between the solute concentrations in the solid matrix and in the solvent might be reached after a certain time (Silva el al., 2007). In this study, the effective range of extraction time was 2–4 hours (120–240 minutes) and the estimated maximum value of TPC was 2,148 mg GAE/100 g corresponding to extraction time of 2.87 hours.
Effect of temperature on TPC of the extracts
Along with the time, temperature is also an important factor of the process of extraction (Pedro el al., 2016). The change in TPC with the variation of extraction temperature was displayed in Table 1.
The results indicated a significant increase in the extraction of total phenolics when increasing the temperature from 25 to 100°C. According to Abad-García el al. (2007), denaturation of membranes and a possible degradation of polyphenolic compounds caused by hydrolysis, internal redox reactions and polymerizations which are detrimental to the extraction yield may happen and influence quantification of bioactive compounds. Cacace and Mazza (2003b) reported that flavonoid families, mainly anthocyanin are heat sensible, hence an upper limit must be respected to avoid degradation of the thermo-sensitive phenolic compounds. For these reasons, TAC of extracts was determined to find out the suitable temperature for extraction process of rice bran. The data were shown in Table 1. The regression equation expressed the relation of extraction temperature and TAC is as follows: 
TAC = –0.0257 (T)2 + 2.0949 (T) + 78.361	(5)
Where, T is extraction temperature (oC).
 Table 1: Effect of extraction temperature on TPC and TAC
Temperature (oC)
TPC (mg GAE/100 g)
TAC (mg CGE/100 g)
25
35
45
65
75
85
100
1,784.3±9.0
1,932.0±12.3
2,130.8±9.0
2,155.1±5.9
2,175.0±9.1
2,190.8±9.0
2,487.7±3.4
114.7±1.4
118.1±1.5
123.9±1.4
105.1±1.1
94.7±1.3
65.5±0.3
33.4±0.4
Values represent the means ± standard deviation, (n=3).

The estimated maximum value of TAC was 121.1 mg CGE/100 g corresponding to extraction temperature of 40.7oC. Abad-García el al. (2007) reported that temperatures above 40◦C produced a decrease in TAC extraction yield. The temperature for TPC extraction from red rice bran was selected as 40oC and the extraction temperature can not be optimized using of mathematical models.
Effect of extraction cycle numbers on TPC of rice bran extract
Table 2: Effect of extraction cycle numbers on TPC 
Extraction cycle numbers
TPC (mg GAE/100g)
1
2
3
4
1,772.4±3.5c
2,138.8±6.0b
2,342.8±5.9a
2,344.8±3.4a
Values represent the means ± standard deviation, (n=3). Values in a column with different superscripts were significantly different (p < 0.05).
Multiple–step extraction is an important method to improve the extraction yield of polyphenols (Chen el al., 2013). The statistical data of TPC corresponding to different numbers of extraction cycles were shown in Table 2. Three cycles of extraction showed significant (p < 0.05) higher yield of TPC than that of using two cycles of extraction. However, four cycles of extraction could not improve the yield of TPC. Similarly, Duong el al. (2015) showed three cycles of extraction is suitable for extraction TPC and TFC from soybean.
Effect of solid–solvent ratio on TPC of rice bran extracts
Solid–solvent ratio may also influence the extraction of phenolic compounds from plant materials (Dai and Mumper, 2010). In this study, this influence was displayed in Table 3. Solid–solvent ratio of 1:6 (w/v) showed high amount of TPC. A further increase in solid–solvent ratio from 1:6 to 1:10 did not significantly (p> 0.05) increase TPC. According Tan el al. (2011), a high solid to solvent ratio was found to be favorable in extraction of phenolic compounds. This high ratio increases concentration gradient, resulting in an increase of diffusion rate that allows greater extraction of solids by solvent (Cacace and Mazza, 2003a). However, active component yields will not continue to increase once equilibrium is reached (Herodež el al., 2003). Duong el al. (2015) obtained a maximum TPC using solid-solvent ratio (1:6, w/v) for soybean extraction.
Table 3: Effect of solid– solvent ratio on TPC 
Solid– solvent ratio
TPC (mg GAE/100g)
1:4
1:5
1:6
1:10
1568.7±3.8c
1770.1±6.6b
2344.6±5.9a
2350.7±7.6a
Values represent the means ± standard deviation, (n=3). Values in a column with different superscripts were significantly different (p < 0.05).
1.11Optimization of extraction conditions
Three parameters: X1, ethanol concentration (48–68%); X2, added acetic acid concentration (10–15%) and X3, extraction time (120–240 min), were able to be optimized using the BBD. The experimental design presented thirty combinations (Table 4), including 3 replicates of the central point in order to estimate pure error and to assess the lack of fit of the proposed models. The multiple regression analysis showed that the model (6) was statistically significant (p < 0.001) and adequately adjusted the experimental data, presenting p lack of fit = 0.22 and R2 = 0.99, which means that the proposed multiple regression model was able and suitable to explain 99% of the variance. 
Y = – 7802.67 + 210.059 X1 + 575.763 X2 + 6.77677 X3 – 1.53312 X12 – 2.5125 X1X2 – 0.0480208 X1X3 – 16.728 X22 – 0.00975 X32		(6)
Where, Y: TPC (mg GAE/100g)
The linear and quadratic terms of all variables significantly influenced the polynomial model for TPC assay (Equation 6). The high values of regression coefficient β2 indicated that the acetic acid concentration had the major influence or importance in the model. The positive values of regression coefficient in the model indicated that an increase of factors tends to increase the response values; on the other hand, negative values indicated that an increase of independent variable tends to decrease the response (Montgomery, 2001). The significance of quadratic term for all variables, which showed negative value, indicated that an increase in these variables for values beyond the studied range tends to decrease the responses or TPC in the extract.

Table 4: The BBD with experimental data for the extraction of TPC from red rice bran
X1
X2
X3
Y (TPC, mg GAE/100g)



Block 1
Block 2
58
48
48
58
48
68
58
48
58
58
58
58
68
68
68
15
15
10
12.5
12.5
12.5
12.5
12.5
10
10
15
12.5
10
15
12.5
120
180
180
180
240
120
180
120
120
240
240
180
180
180
240
2231.5
2296.6
2107.4
2367.5
2308.4
2131.0
2385.2
2213.8
2184.2
2243.4
2284.7
2373.4
2066.0
2012.8
2101.5
2219.7
2302.5
2107.4
2385.2
2302.5
2125.1
2379.3
2213.8
2178.3
2255.2
2296.6
2367.5
2060.1
1995.1
2107.4

The response surfaces for ethanol concentration and acetic acid concentration variables versus the extraction time were shown in Figure 4. According to the results, all surface responses showed a concave characteristic, which is in agreement with the significance of quadratic. Additionally, it can be observed from Figure 4 that maximum values of responses were found between the ethanol concentration of 52–56%, acetic acid concentraion of 12.5–3.5% and extraction time of 200–220 min. This probably occurred due to solubility of polyphenol compounds in the mixture solution of water and ethanol since the antioxidant extraction depends of the solvent polarity (Tan el al., 2013). These results shared similarities with the results reported by Chooklin (2014), which evaluated the extraction of phenolic compounds from brown rice extract from Sung Yod Phatthalung, using mixture solution of water and ethanol with pH adjustment as solvent.



Figure 4: Response surface plot of ethanol concentration and acetic acid concentration versus the extraction time on the TPC in the extract

The optimum level of TPC was showed in Table 5. The extraction conditions were as follows: ethanol concentration of 54.5%, added acetic acid concentration of 13% and extraction time of 210 min. A triplicate experiment was set up to validate the optimized conditions. As shown in Table 5, the experimental TPC of red rice bran was 2,391.1±5.9 mg GAE/100 g and was in good agreement with the predicted value. Goffman and Bergman (2004) analysed TPC of rice bran from 10 red rice varieties and reported that the TPC were in ranges of 2,600–4,390 mg GAE/100 g. While, Zhang el al. (2010) declared the result with great fluctuation that total phenolic contents of bran samples from 12 diverse varieties of black rice ranged from 2,365 to 7,367 mg GAE/100 g of dry weight. The composition of rice bran (including phenolic compounds) widely varies depending on the source of bran (cultivars), the milling degrees, the used stabilization techniques, and the basis of grain size: long, medium, or short grain (Lloyd el al., 2000). Besides, the growth period as well as other environmental factors, i.e. humidity, soil texture, quality and quantity of water, etc. have significant effect on its composition and antioxidant properties (Iqbal el al., 2005).

Table 5: Optimum extraction conditions and the values of TPC
Factors
Optimum conditions
Ethanol concentration (%)
Acetic acid concentration (%)
Extraction time (min)
54.5
13.1
210
TPC (Predicted) (mg GAE/100g)
TPC (Experiment1)
2,399.6a
2,391.1a±5.9
(1): Data are presented as mean from triplicate of experimental run ± SD. The same letters within row indicate not significant difference by t’ test from hypothesis for mean at 5 % level.  

The verification value for TPC obtained are not significantly different with predicted values (Table 5) which clearly showed that the model fitted the experimental data very well and therefore optimized the phenolic extraction efficiently within the specified range of process parameters. 
Using optimized conditions for phenolic extraction, the antioxidant activity of extract was determined as 108.1±2.9 (µg/mL). The IC50 value of this study is in agreement with the result from the research of (Jun el al., 2012) on antioxidant activitie of red rice bran in term of IC50 value which was 112.6 μg/mL. 
1.12CONCLUSIONS
Based on the experiment that had been done, the experimental value agreed with the predicted one. The optimal TPC was determined at ethanol with concentration of 54.5%, added acetic acid concentration of 13.1%, extraction time of 210 min at 40oC, the solid–solvent ratio 1:6 (w/v) and three cycles of extraction. Optimization of the extraction procedure using RSM showed data adjusted to the statistic models. The extract of red rice bran exhibited high TPC and antioxidant activity. Therefore, is might act as a potential natural antioxidant source.
REFERENCES
Abad-García, B., Berrueta, L., López-Márquez, D., Crespo-Ferrer, I., Gallo, B. and Vicente, F., 2007. Optimization and validation of a methodology based on solvent extraction and liquid chromatography for the simultaneous determination of several polyphenolic families in fruit juices. Journal of Chromatography A 1154(1): 87-96.
Baş, D. and Boyacı, İ. H., 2007. Modeling and optimization I: Usability of response surface methodology. Journal of Food Engineering 78(3): 836-845.
Bassani, D. C., Nunes, D. S. and Granato, D., 2014. Optimization of phenolics and flavonoids extraction conditions and antioxidant activity of roasted yerba-mate leaves (Ilex paraguariensis A. St.-Hil., Aquifoliaceae) using response surface methodology. Anais da Academia Brasileira de Ciências 86(2): 923-934.
Boeing, J. S., Barizão, É. O., de Silva, B. C., Montanher, P. F., de Cinque Almeida, V. and Visentainer, J. V., 2014. Evaluation of solvent effect on the extraction of phenolic compounds and antioxidant capacities from the berries: application of principal component analysis. Chemistry Central Journal 8(1): 1-9.
Cacace, J. E. and Mazza, G., 2003a. Mass transfer process during extraction of phenolic compounds from milled berries. Journal of Food Engineering 59(4): 379-389.
Cacace, J. E. and Mazza, G., 2003b. Optimization of extraction of anthocyanins from black currants with aqueous ethanol. Journal of Food Science 68(1): 240-248.
Chen, M.-H. and Bergman, C. J., 2005. A rapid procedure for analysing rice bran tocopherol, tocotrienol and γ-oryzanol contents. Journal of Food Composition and Analysis 18(4): 319-331.
Chen, Q. M., Fu, M. R., Yue, F. L. and Cheng, Y. Y., 2015. Effect of Superfine Grinding on Physicochemical Properties, Antioxidant Activity and Phenolic Content of Red Rice (Oryza sativa L.). Food and Nutrition Sciences 6(14): 1277.
Chen, X., Wu, X., Chai, W., Feng, H., Shi, Y., Zhou, H. and Chen, Q., 2013. Optimization of extraction of phenolics from leaves of Ficus virens. Journal of Zhejiang University Science B 14(10): 903-915.
Chooklin, S., 2013. Ultrasound-assisted extraction of phenolic compounds from brown rice and their antioxidant activities. Kasetsart Journal - Natural Science 47: 864-873.
Chooklin, S., 2014. Extraction of brown rice extract and application in refined palm olein during accelerated storage. International Food Research Journal 21(1): 291-296.
Dai, J. and Mumper, R. J., 2010. Plant phenolics: extraction, analysis and their antioxidant and anticancer properties. Molecules 15(10): 7313-7352.
de Mira, N. V. M., Massaretto, I. L., Pascual, C. d. S. C. I. and Marquez, U. M. L., 2009. Comparative study of phenolic compounds in different Brazilian rice (Oryza sativa L.) genotypes. Journal of Food Composition and Analysis 22(5): 405-409.
Do, Q. D., Angkawijaya, A. E., Tran, N. P. L., Huynh, L. H., Soetaredjo, F. E., Ismadji, S. and Ju, Y.-H., 2014. Effect of extraction solvent on total phenol content, total flavonoid content, and antioxidant activity of Limnophila aromatica. Journal of Food and Drug Analysis 22(3): 296-302.
Duong, T. P. L., Phan, T. B. T. and Ha, T. T., 2015. Effects of extraction process on phenolic content and antioxidant activity of soybean. Journal of Food and Nutrition Sciences 3(Special Issue: Food Processing and Food Quality): 33-38.
Escribano-Bailón, M. T., Santos-Buelga, C. and Rivas-Gonzalo, J. C., 2004. Anthocyanins in cereals. Journal of Chromatography A 1054(1): 129-141.
Goffman, F. and Bergman, C., 2004. Rice kernel phenolic content and its relationship with antiradical efficiency. Journal of the Science of Food and Agriculture 84(10): 1235-1240.
Granato, D., De Castro, I. A., Ellendersen, L. and Masson, M. L., 2010. Physical Stability Assessment and Sensory Optimization of a Dairy‐Free Emulsion Using Response Surface Methodology. Journal of Food Science 75(3).
Granato, D., de Araújo Calado, V. M. and Jarvis, B., 2014a. Observations on the use of statistical methods in food science and technology. Food Research International 55: 137-149.
Granato, D., Grevink, R., Zielinski, A. c. A., Nunes, D. S. and van Ruth, S. M., 2014b. Analytical strategy coupled with response surface methodology to maximize the extraction of antioxidants from ternary mixtures of green, yellow, and red teas (Camellia sinensis var. sinensis). Journal of Agricultural and Food Chemistry 62(42): 10283-10296.
Herodež, Š. S., Hadolin, M., Škerget, M. and Knez, Ž., 2003. Solvent extraction study of antioxidants from Balm (Melissa officinalis L.) leaves. Food Chemistry 80(2): 275-282.
Huang, Y.-P. and Lai, H.-M., 2016. Bioactive compounds and antioxidative activity of colored rice bran. Journal of Food and Drug Analysis 24(3): 564-574.
Iqbal, S., Bhanger, M. and Anwar, F., 2005. Antioxidant properties and components of some commercially available varieties of rice bran in Pakistan. Food Chemistry 93(2): 265-272.
Itani, T. and Ogawa, M., 2004. History and recent trends of red rice in Japan. Japanese Journal of Crop Science 73(2): 137-147.
Jun, H. I., Song, G. S., Yang, E. I., Youn, Y. and Kim, Y. S., 2012. Antioxidant activities and phenolic compounds of pigmented rice bran extracts. Journal of Food Science 77(7).
Lee, J. H., 2010. Identification and quantification of anthocyanins from the grains of black rice (Oryza sativa L.) varieties. Food Science and Biotechnology 19(2): 391-397.
Lloyd, B., Siebenmorgen, T. and Beers, K., 2000. Effects of commercial processing on antioxidants in rice bran. Cereal Chemistry 77(5): 551-555.
Michiels, J. A., Kevers, C., Pincemail, J., Defraigne, J. O. and Dommes, J., 2012. Extraction conditions can greatly influence antioxidant capacity assays in plant food matrices. Food Chemistry 130(4): 986-993.
Min, B., Gu, L., McClung, A. M., Bergman, C. J. and Chen, M.-H., 2012. Free and bound total phenolic concentrations, antioxidant capacities, and profiles of proanthocyanidins and anthocyanins in whole grain rice (Oryza sativa L.) of different bran colours. Food Chemistry 133(3): 715-722.
Montgomery, D. C., 2001. Design and analysis of experiments. Fifth edition. John Wiley & Sons. 684 pages.
Muntana, N. and Prasong, S., 2010. Study on total phenolic contents and their antioxidant activities of Thai white, red and black rice bran extracts. Pakistan Journal of Biological Sciences 13(4): 170.
Oki, T., Masuda, M., Kobayashi, M., Nishiba, Y., Furuta, S., Suda, I. and Sato, T., 2002. Polymeric procyanidins as radical-scavenging components in red-hulled rice. Journal of Agricultural and Food Chemistry 50(26): 7524-7529.
Pedro, A. C., Granato, D. and Rosso, N. D., 2016. Extraction of anthocyanins and polyphenols from black rice (Oryza sativa L.) by modeling and assessing their reversibility and stability. Food Chemistry 191: 12-20.
Pereira-Caro, G., Watanabe, S., Crozier, A., Fujimura, T., Yokota, T. and Ashihara, H., 2013. Phytochemical profile of a Japanese black–purple rice. Food Chemistry 141(3): 2821-2827.
Pinelo, M., Rubilar, M., Sineiro, J. and Nunez, M., 2004. Extraction of antioxidant phenolics from almond hulls (Prunus amygdalus) and pine sawdust (Pinus pinaster). Food Chemistry 85(2): 267-273.
Rao, A. S. V. C., Reddy, S. G., Babu, P. P. and Reddy, A. R., 2010. The antioxidant and antiproliferative activities of methanolic extracts from Njavara rice bran. BMC Complementary and Alternative Medicine 10(1): 4.
Shao, Y., Xu, F., Sun, X., Bao, J. and Beta, T., 2014. Identification and quantification of phenolic acids and anthocyanins as antioxidants in bran, embryo and endosperm of white, red and black rice kernels (Oryza sativa L.). Journal of Cereal Science 59(2): 211-218.
Silva, E. M., Rogez, H. and Larondelle, Y., 2007. Optimization of extraction of phenolics from Inga edulis leaves using response surface methodology. Separation and Purification Technology 55(3): 381-387.
Sripakdee, T., Mahachai, R. and Chanthai, S., 2017. Direct analysis of anthocyanins-rich Mao fruit juice using sample dilution method based on chromophores/fluorophores of both cyanidin-3-glucoside and pelargonidin-3-glucoside. International Food Research Journal 24(1): 215-222.
Tabaraki, R. and Nateghi, A., 2011. Optimization of ultrasonic-assisted extraction of natural antioxidants from rice bran using response surface methodology. Ultrasonics sonochemistry 18(6): 1279-1286.
Tan, M. C., Tan, C. P. and Ho, C., 2013. Effects of extraction solvent system, time and temperature on total phenolic content of henna (Lawsonia inermis) stems. International Food Research Journal 20(6): 3117-3123.
Tan, P., Tan, C. and Ho, C., 2011. Antioxidant properties: Effects of solid-to-solvent ratio on antioxidant compounds and capacities of Pegaga (Centella asiatica). International Food Research Journal 18(2): 557-562.
Yawadio, R., Tanimori, S. and Morita, N., 2007. Identification of phenolic compounds isolated from pigmented rices and their aldose reductase inhibitory activities. Food Chemistry 101(4): 1616-1625.
Yoshida, H., Tomiyama, Y. and Mizushina, Y., 2010. Lipid components, fatty acids and triacylglycerol molecular species of black and red rices. Food Chemistry 123(2): 210-215.
Zhang, M. W., Zhang, R. F., Zhang, F. X. and Liu, R. H., 2010. Phenolic profiles and antioxidant activity of black rice bran of different commercially available varieties. Journal of Agricultural and Food Chemistry 58(13): 7580-7587.
