﻿
DOI:10.22144/ctu.jvn.2020.079
BÀI TOÁN LIÊN THÔNG P-MEDIAN TRÊN ĐỒ THỊ ĐẦY ĐỦ VÀ ĐỒ THỊ LƯỠNG PHÂN ĐẦY ĐỦ
Nguyễn Ngọc Đăng Duy1* và Võ Nguyễn Minh Hiếu2
1Sinh viên Sư phạm Toán học, Khóa 43, Trường Đại học Cần Thơ
1Sinh viên Sư phạm Toán học, Khóa 42, Trường Đại học Cần Thơ
*Người chịu trách nhiệm về bài viết: Nguyễn Ngọc Đăng Duy (email: duy3300@gmail.com)
Thông tin chung:
Ngày nhận bài: 31/03/2020
Ngày nhận bài sửa: 09/06/2020
Ngày duyệt đăng: 28/08/2020

Title: 
Connected p-median problem on complete graphs and complete bipartite graphs
Từ khóa: 
Bài toán p-median,  đồ thị đầy đủ, đồ thị lưỡng phân đầy đủ, thuật toán thời gian tuyến tính
Keywords:
P-median problem, complete graph, complete partite graph, linear-time algorithm   ABSTRACT
In this paper, a connected p-median problem on complete graphs and complete bipartite graphs is mentioned. To solve this problem, several theorems and lemmas are given during research. Besides, linear-time algorithms are developed to solve the connected p-median problem on complete graphs and complete bipartite graphs.
   TÓM TẮT
Trong bài báo này, một bài toán vị trí liên quan đến các thành phần liên thông trên đồ thị đầy đủ và đồ thị lưỡng phân đầy đủ được đề cập. Để giải quyết bài toán này, một số định lí và bổ đề được đưa ra trong quá trình nghiên cứu. Bên cạnh đó, các thuật toán thời gian tuyến tính được đưa ra để giải bài toán liên thông p-median trên đồ thị đầy đủ và đồ thị lưỡng phân đầy đủ.Trích dẫn: Nguyễn Ngọc Đăng Duy và Võ Nguyễn Minh Hiếu, 2020. Bài toán liên thông p-median trên 
đồ thị đầy đủ và đồ thị lưỡng phân đầy đủ. Tạp chí Khoa học Trường Đại học Cần Thơ. 56(4A): 26-32.
   1 MỞ ĐẦU
	Bài toán vị trí khởi nguồn từ bài toán nổi tiếng được đưa ra bởi nhà toán học Fermat (1607-1665) vào khoảng thế kỷ XVII, đó là tìm vị trí của một điểm mới trên mặt phẳng sao cho khoảng cách từ nó đến ba điểm cho trước là nhỏ nhất. Bài toán đó sau này đã được giải bởi Torricelli (Krarup and Vajda, 1997). Từ bài toán này, trường hợp đối với một tập gồm điểm đã được đưa ra và điểm làm tối thiểu hàm khoảng cách đến tất cả các điểm còn lại được gọi là điểm median trên mặt phẳng, và hàm cần làm tối thiểu gọi là hàm median (Kariv and Hakimi, 1979).
	Bài toán vị trí nói trên đã được áp dụng vào các loại đồ thị đặc biệt như đồ thị cây, đồ thị có dạng mạng lưới,... tuy nhiên vì một lí do đặc biệt nào đó mà người ta cần tìm nhiều hơn một cơ sở mới trên mạng lưới đồ thị sao cho hàm khoảng cách từ các điểm có sẵn trên mặt phẳng đến tập hợp các điểm đó là nhỏ nhất, mặt khác các điểm này được đòi hỏi phải là các điểm liên thông, từ đây một lớp các bài toán đã được đưa ra nghiên cứu. 
   Nghiên cứu của Chang et al. (2015) về bài toán liên thông p-median trên đồ thị khối, các tác giả chứng minh rằng bài toán liên thông p-median trên đồ thị khối với độ dài tổng quát là NP-khó. Trong trường hợp đặc biệt, khi đồ thị khối có độ dài các cạnh bằng nhau thì tác giả cũng chỉ ra rằng bài toán liên thông p-median có thể giải trong thời gian tuyến tính. 
   Công trình nghiên cứu của Kang et al. (2016) về bài toán liên thông p-centdian trên đồ thị khối, các tác giả đã xem xét lại bài toán p-median liên thông với phương pháp giải đơn giản hóa và sau đó giải bài toán hai mục tiêu p-median và p-center (gọi tắt là p-centdian) trong thời gian O(n2), trong đó n là số đỉnh của đồ thị khối.
   Trong bài báo này, tiếp nối những kết quả của các tác giả trên, bài báo này đặt ra một vấn đề về bài toán vị trí liên thông p-median trên đồ thị đầy đủ và đồ thị lưỡng phân đầy đủ. 
   2 GIỚI THIỆU VỀ BÀI TOÁN
   2.1 Một số khái niệm có liên quan
	Theo Bondy and Murty (1976), đồ thị  được định nghĩa là một bộ phận hợp thành bởi ba thành phần  trong đó   là một tập không rỗng chứa các đỉnh của ,  là một tập chứa các cạnh của ,  rời nhau với  ,   là một ánh xạ liên kết mỗi cạnh của  với một cặp đỉnh của . Nếu ta gọi là một cạnh của đồ thị  và  là hai đỉnh sao cho  thì ta nói  là cạnh nối  với . Khi đó  liên thuộc với nhau;  và  được gọi là các điểm nút của . Một đồ thị được gọi là đồ thị đơn nếu nó không có cạnh nào có điểm đầu và điểm cuối trùng nhau và không có hai cạnh nào nối cùng một cặp đỉnh.
   Hai đỉnh của một đồ thị được gọi là kề nhau nếu tồn tại một cạnh nối hai điểm đó
   Một đồ thị được gọi là đồ thị đầy đủ (Hình 1) nếu nó là đồ thị đơn và hai đỉnh bất kỳ của đồ thị luôn kề nhau.

Hình 1: Đồ thị đầy đủ
   Đồ thị lưỡng phân là một đồ thị mà trong đó tập hợp các đỉnh của nó có thể được chia thành hai tập con rời nhau sao cho hai đỉnh thuộc cùng một tập con thì không kề nhau. Đồ thị lưỡng phân đầy đủ (Hình 2) là một đồ thị lưỡng phân mà trong đó bất kì một đỉnh nào từ tập con này, đều kề với tất cả các đỉnh thuộc tập con kia. 

Hình 2: Đồ thị lưỡng phân đầy đủ
    Đường đi độ dài  từ đỉnhđến đỉnhlà dãy  trong đó  là số nguyên dương,  và .
   Trong bài báo này, ta chỉ đề cập đến các đồ thị đơn mà các cạnh của nó có độ dài đơn vị, nghĩa là  với mọi  và khoảng cách giữa hai điểm  là độ dài đường đi độ dài ngắn nhất nối và .
   Một tập hợp con  của  là liên thông nếu luôn có một đường đi nối hai đỉnh bất kỳ trong .
   Bài báo này đề cập đến một bài toán là tìm một tập hợp liên thông  gồm  đỉnh ( với  là số đỉnh của đồ thị đầy đủ  ),  là tập con của  sao cho tổng khoảng cách có trọng số từ những đỉnh thuộc  đến   là nhỏ nhất , nghĩa là làm tối thiểu hàm mục tiêu:
   
   với  và  là trọng số của đỉnh .
   3 BÀI TOÁN LIÊN THÔNG P-MEDIAN TRÊN ĐỒ THỊ ĐẦY ĐỦ
   Bây giờ ta xét bài toán liên thông p-median trên đồ thị đầy đủ với trường hợp . Khi đó, đây là bài toán 1-median cổ điển trên đồ thị đầy đủ với hàm mục tiêu là:
   
   Ta cần tìm  sao cho  đạt min.
   Do ta đang xét đồ thị đầy đủ với độ dài đơn vị nên khoảng cách giữa hai đỉnh phân biệt bất kỳ thuộc đồ thị đều bằng 1. Do đó hàm mục tiêu ta đang xét là hàm:

   Với mỗi tập , ta đặt , khi đó  là một hằng số và . Dễ thấy  với  là đỉnh có trọng số lớn nhất trên đồ thị  sẽ làm cho hàm mục tiêu đạt giá trị nhỏ nhất,  nghĩa là tập hợp liên thông, trong đó hàm trả về đỉnh có trọng số lớn nhất.                                                                  
   Đối với trường hợp , bài toán tương ứng là bài toán tìm tập hợp gồm  đỉnh liên thông sao cho hàm median sau đây đạt giá trị nhỏ nhất:
   
   Giả sử ta có   là tập hợp gồm  điểm lấy từ  sao cho hàm median  đạt giá trị nhỏ nhất. Khi đó ta có bổ đề sau đây:
   Bổ đề 3.1: Đánh chỉ số các đỉnh của  theo thứ tự trọng số giảm dần, nghĩa là  thì khi đó p-median liên thông của  là .
   Chứng minh
   Với mọi tập hợp  và , ta luôn có:
   
   Bổ đề được chứng minh.           	                  
   Theo kết quả có được ở Bổ đề 3.1, ta chứng minh được tập liên thông p-median của  là tập hợp  đỉnh có trọng số lớn nhất thuộc . Từ đây, ta xây dựng thuật toán tổ hợp để tìm tập hợp liên thông p-median  trên đồ thị đầy đủ  cho trước. Ý tưởng của thuật toán là xem xét điểm trung vị của một dãy các trọng số đỉnh cho trước và xét tập gồm các phần tử lớn hơn hoặc bằng điểm trung vị đó. Nếu ta thu được một tập có số phần tử lớn hơn p, khi đó ta tiếp tục tìm trung vị của dãy gồm các phần tử trong tập mới. Ngược lại, nếu số phần tử trong tập này nhỏ hơn p, ta tìm trung vị của tập còn lại để bổ sung các phần tử cho tập đang xét. Như vậy, mỗi lần lặp ta thu được số phần tử bằng một nửa số phần tử đang xét. Hơn nữa, thuật toán tìm trung vị của một dãy có độ phức tạp bằng độ dài của dãy đó (Hoare, 1961). Giả sử độ phức tạp của bài toán là T(n) với n là số đỉnh của G, khi đó, 
   	                                        (*)
   với k là số bước lặp của thuật toán thỏa   và kí hiệu  là phần nguyên của phần tử x. Vì T(c) = 1 với  nên từ biểu thức (*) suy ra T(n) là một hàm bậc nhất theo n. Nói một cách khác, thuật toán chạy trong thời gian tuyến tính.
   Sau đây, chúng ta xem xét một thuật toán thời gian tuyến tính để tìm tập hợp  gồmsố lớn nhất trong một tập hợp gồm  số .
   Thuật toán 3.2: Tìm  số lớn nhất trong một tập hợp  gồm  phần tử là số thực 
   Input: Một tập hợp  gồm  phần tử là số. 
   While  do
   med:= trung vị của B
   
   
   
   If  do 
   Đặt .
   Else
   If  do  và chọn   phần tử trong  để nối vào .
   Else
   Đặt  và 
   		Endif
   	Endif
   Endwhile
   Output: Tập hợp  gồm  số lớn nhất lấy từ .
   Ví dụ 3.3: Tìm tập hợp  gồm 5 số lớn nhất lấy từ tập hợp  
   Áp dụng Thuật toán 3.2, ta có kết quả như sau:
   Vòng lặp số 1:
   Ta có  nên   
 
Vì và  nên 
   Đặt  và  
   Vòng lặp số 2:
   Ta có  nên  
    
   Vì  và nên
    và chọn   phần tử thuộc  để nối vào 
   Đến đây  nên Thuật toán 3.2 dừng.
   Vậy ta thu được tập hợp  cần tìm là .
   Sau đây, bằng cách áp dụng Thuật toán 3.2, chúng tôi đề xuất một thuật toán để giải bài toán liên thông p-median trên đồ thị đầy đủ.
   Thuật toán 3.4: Giải bài toán liên thông p-median trên đồ thị đầy đủ.
   Input: Đồ thị đầy đủ  với  đỉnh
   Đặt  với tập trọng số tương ứng 
   Áp dụng Thuật toán 3.2 để tìm tập hợp  gồm p số lớn nhất lấy từ tập hợp . 
   Output: Tập   gồm p đỉnh liên thông tương ứng với tập .
   Ví dụ 3.5: Cho đồ thị đầy đủ như Hình 3, với trọng số cho bởi Bảng 3. Ta giải bài toán với 
Bảng 3: Trọng số các đỉnh
 1234546872   
Hình 3: Đồ thị đầy đủ 
   Ta có  
   Áp dụng Thuật toán 3.2, ta được:
   . 
   Vậy  hay .
   Định lý 3.6: Bài toán liên thông  p-median trên đồ thị đầy đủ có thể giải trong thời gian tuyến tính.
   4 BÀI TOÁN LIÊN THÔNG  P-MEDIAN TRÊN ĐỒ THỊ LƯỠNG PHÂN ĐẦY ĐỦ
   Bây giờ, ta xét bài toán liên thông p-median trên đồ thị lưỡng phân đầy đủ.
   Bài toán của chúng ta trong trường hợp này có thể được phát biểu như sau: Tìm tập hợp liên thông p-median trên đồ thị lưỡng phân đầy đủ  sao cho hàm median sau đây đạt giá trị nhỏ nhất:
   
   với , hay:
   
	Đối với trường hợp , bài toán trên cũng là bài toán 1-median cổ điển trên đồ thị lưỡng phân đầy đủ, đó là tìm một đỉnh trên đồ thị này sao cho tổng khoảng cách từ các đỉnh còn lại thuộc đồ thị đến nó là nhỏ nhất.
   Theo định nghĩa của đồ thị lưỡng phân đầy đủ, ta có thể chia tập hợp đỉnh của  thành hai tập rời nhau  và  sao cho hai đỉnh thuộc cùng một tập thì không kề nhau.
   Đặt  và . Nếu  thì  là điểm 1-median, ngược lại thì  là điểm 1-median.
   Bây giờ ta xét trường hợp  , khi đó bài toán của chúng ta là tìm tập hợp liên thông 2-median trên đồ thị lưỡng phân đầy đủ sao cho hàm median
   
   đạt giá trị nhỏ nhất. 
   Vì  liên thông nên  gồm hai đỉnh, trong đó có một đỉnh thuộc  và  một đỉnh thuộc .
   Dễ thấy  do  là tập hợp liên thông, do đó hàm median của chúng ta là hàm:
   
   Ta xét mệnh đề sau:
   Mệnh đề 4.7: Tập liên thông 2-median  của  thỏa gồm hai đỉnh là  và .
   Chứng minh
   Đặt , khi đó  
   và  là tập liên thông thì khi đó  với  , ta luôn có:
   Như vậy ta luôn có  với mọi . Mệnh đề được chứng minh.                                                                                                     
   Như vậy với  thì tập liên thông p-median   là tập gồm 2 điểm là trọng số lớn nhất lần lượt nằm trong hai thành phần phân chia  và .
   Bằng phép chứng minh tương tự như Mệnh đề 4.7, ta chứng minh được với  , tập liên thông  là tập liên thông 2-median hợp với tập hợp gồm  đỉnh nữa là các đỉnh có trọng số lớn nhất trên đồ thị lưỡng phân đầy đủ  sau khi đã bỏ đi các đỉnh thuộc tập 2-median. Thật vậy, ta có chứng minh như sau:
   Theo như Mệnh đề 4.7, ta có tập liên thông  với  chứa hai đỉnh có trọng số lớn nhất lần lượt nằm trên hai thành phần phân chia của đồ thị . Với , ta xét tập liên thông  gồm hai đỉnh ,  và  đỉnh có trọng số lớn nhất trên đồ thị , khi đó ,  liên thông và , ta có:
Như vậy  với mọi tập liên thông  và                        
	Từ đây ta sẽ đi xây dựng thuật toán thời gian tuyến tính để giải bài toán liên thông p-median trên đồ thị lưỡng phân đầy đủ . Ý tưởng của thuật toán này là sử dụng Thuật toán 3.2 để tìm ra đỉnh có trọng số lớn nhất lần lượt nằm trên hai thành phần phân chia, và sau đó, ta thêm các đỉnh có trọng số lớn hơn median của các đỉnh còn lại để thêm vào tập  cho đến khi ta có  đỉnh thuộc .  
   Thuật toán 4.8: Giải bài toán liên thông p-median trên đồ thị lưỡng phân đầy đủ .
   Input: Đồ thị lưỡng phân đầy đủ với  và , 
   Gọi  là tập hợp chứa 1 đỉnh có trọng số lớn nhất trong 
   Áp dụng Thuật toán 3.2 để tìm 
    Gọi  là tập hợp chứa 1 đỉnh có trọng số lớn nhất trong 
   Áp dụng Thuật toán 3.2 để tìm 
   	 
   	Đặt 
   Áp dụng Thuật toán 3.2  để tìm tập hợp  gồm  đỉnh có trọng số lớn nhất lấy từ 
   Đặt 
   Output: Tập   gồm p đỉnh liên thông tương ứng với tập .
   Ví dụ 4.9: Cho đồ thị lưỡng phân đầy đủ như trong Hình 4 và trọng số cho bởi Bảng 4. Ta giải bài toán với 
Bảng 4: Trọng số các đỉnh
 12345678 15286934
Hình 4: Đồ thị lưỡng phân đầy đủ
   Đồ thị lưỡng phân đầy đủ đã cho có  và . 
   . 
   Đặt , áp dụng Thuật toán 3.2 với , ta thu được  
    Đặt , áp dụng Thuật toán 3.2 với , ta thu được  
   
   Đặt 
   Tiếp tục áp dụng Thuật toán 3.2 với tập hợp  để tìm tập hợp  gồm đỉnh có trọng số lớn nhất trong , ta có được    
   Đặt 
   Vậy  và do đó .
   Định lí 4.10: Bài toán liên thông p-median trên đồ thị lưỡng phân đầy đủ có thể giải trong thời gian tuyến tính.
   5 KẾT LUẬN
   Trong bài báo này, các thuật toán thời gian tuyến tính đã được đưa ra để giải bài toán liên thông p-median trên đồ thị đầy đủ và đồ thị lưỡng phân đầy đủ. Trong các bài báo kế tiếp chúng ta có thể nghiên cứu tìm một thuật toán thời gian tuyến tính để giải bài toán liên thông trên các dạng đồ thị khác, tiêu biểu là đồ thị có nhiều hơn hai thành phần phân chia, đồ thị đa lớp, các loại đồ thị có trọng số dương/âm
   TÀI LIỆU THAM KHẢO
Bondy, J.A. , and Murty, U.S.R. , 1976. Graph theory with applications. The Macmillan Press Ltd. Great Britain, 264 pages.
Chang, S.C., Yen, W.C.K., Wang, Y.L., and Liu, J.J., 2015. The connected p-median problem on block graphs. Springer - Verlag.
Hoare, C.A.R., 1961.  Algorithm 65: Find . Communications of the ACM, 4(7): 321-322.
Kang, L., Zhou, J. and Shan, E., 2018 Algorithms for connected p-centdian problem on block graphs. J Comb Optim, 36(1): 252-263.
Kariv, O., and Hakimi, S.L., 1979. An algorithmic approach to network location problems, II. The p-medians. SIAM Journal on Applied Mathematics, 37(3): 539-560.
Krarup, J., and Vajda, S., 1997. On Torricelli's geometrical solution to a problem of Fermat IMA Journal of Mathematics Applied in Business and Industry, 8(3): 215-224.
   
Tạp chí Khoa học Trường Đại học Cần Thơ 	Tập 56, Số 4A (2020): 26-32

7

