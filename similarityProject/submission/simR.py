import os
import sys
from collections import Counter
import math
import re
import tachtu
from decimal import Decimal
import time
import cosinetfidf
import datetime

# ---------------Xóa các từ trùng nhau ---------------


def xoa_trung_nhau(fileTxt):
    file_input = open(fileTxt, "r+", encoding="utf-8")
    read_file = file_input.read()
    # Phân tách dữ liệu nhận được từ kiểu String chuyển sang danh sách List
    words = read_file.split(' ')
    count_each_word = Counter(words)
    # Loại bỏ các từ lặp lại trong file Text
    del_word = dict(count_each_word)
    return list(del_word.keys())


# ---------------Tập từ phân biệt của mỗi file ---------------
def tap_tu_phan_biet(fileText_1, fileText_2):
    tap_tu = []
    # Hợp hai tập fileText lại với nhau
    # fileText_1.extend(fileText_2)
    phepHop = fileText_1 + fileText_2
    # Loại bỏ từ trùng nhau
    for i in phepHop:
        if i in tap_tu:
            pass
        else:
            tap_tu.append(i)
    return tap_tu


# # ---------------Vector thứ tự từ ---------------
def vector_thu_tu_tu(tap_tu, file):
    vector = []
    vitri = 0
    for i in range(len(tap_tu)):
        for j in range(len(file)):
            if tap_tu[i] == file[j]:
                vitri = j + 1
                vector.append(j)
            if vitri == 0 and j == (len(file) - 1):
                vitri = 0
                vector.append(vitri)
        vitri = 0
    return vector


def SIMR(file_1, file_2):
    # Đọc và xóa các phần tử (từ) trùng nhau của cả 2 File
    temp1 = xoa_trung_nhau(file_1)
    temp2 = xoa_trung_nhau(file_2)
    # print("temp1: " + str(temp1))
    # print("temp2: " + str(temp2))

    # Hợp 2 list (list temp1 và list temp2) lại với nhau để tạo ra "tập từ phân biệt"
    tap_tu = tap_tu_phan_biet(temp1, temp2)
    # print("-----------Tập từ Phân biệt: \n" + str(tap_tu))

    # Vector thứ tự từ
    vector_1 = vector_thu_tu_tu(tap_tu, temp1)
    vector_2 = vector_thu_tu_tu(tap_tu, temp2)
    # print("Vector thứ tự từ 1: " + str(vector_1))

    # print("Vector thứ tự từ 2: " + str(vector_2))
    # print(len(temp1))
    # print(len(temp2))
    # print(len(tap_tu))
    # print(len(vector_1))
    # print(len(vector_2))

    # Hiệu số simR
    hieuSo = 0
    if len(vector_1) == len(vector_2):
        for i in range(len(vector_1)):
            hieuSo += (vector_1[i] - vector_2[i]) * (vector_1[i] - vector_2[i])
    # Căn bậc 2 của hiệu số
    canBac_hieuSo = math.sqrt(hieuSo)

    # Tổng số simR
    tongSo = 0
    if len(vector_1) == len(vector_2):
        for i in range(len(vector_1)):
            tongSo += (vector_1[i] + vector_2[i]) * (vector_1[i] + vector_2[i])
    # Căn bậc 2 của hiệu số
    canBac_tongSo = math.sqrt(tongSo)

    # Tính simR (độ tương đồng thứ tự từ)
    simR = (1 - (canBac_hieuSo / canBac_tongSo))
    # print("Độ tường đồng thứ tự từ: ", simR)
    return simR

# file1 = r"../Kho/ENG/A-KHTN/01-NS-HUYNH NHU THAO(1-8)009.txt"
# file2 = r"../Kho/VIE/A-KHTN/01-H8-NGUYEN CUONG QUOC(1-9)105.txt"
# vector1 = cosinetfidf.text_to_vector(file1)
# vector2 = cosinetfidf.text_to_vector(file2)
# cosine = cosinetfidf.get_cosine(vector1, vector2)
# simOrder = SIMR(file1 , file2)*1.1
# totalSim = (0.5 * cosine) + (0.5 * simOrder)
# print("Tương đồng Cosine TF-IDF:", round(cosine, 3))
# print("Tương đồng thứ tự từ:",round(simOrder,3))
# print("Tổng Similarity:", round(totalSim,3))