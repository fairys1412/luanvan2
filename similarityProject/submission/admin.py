from django.contrib import admin
from .models import  User
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.utils.html import mark_safe  # xử lý các tập tin media

admin.site.register(User)