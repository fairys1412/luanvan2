CREATE SEQUENCE taptin_id_seq;
ALTER SEQUENCE taptin_id_seq OWNED BY submission_taptin.file_ma;
ALTER TABLE submission_taptin ALTER COLUMN file_ma SET DEFAULT nextval('taptin_id_seq');