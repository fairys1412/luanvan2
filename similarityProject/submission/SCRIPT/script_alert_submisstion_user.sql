CREATE SEQUENCE user_id_seq;
ALTER SEQUENCE user_id_seq OWNED BY submission_user.id;
ALTER TABLE public.submission_user ALTER COLUMN id SET DEFAULT nextval('user_id_seq');

UPDATE submission_user
SET id = nextval('user_id_seq')
WHERE username = 'admin';

UPDATE submission_user
SET id = nextval('user_id_seq')
WHERE username = 'pngan@1234';

UPDATE submission_user
SET id = nextval('user_id_seq')
WHERE username = 'tacgia2';

UPDATE submission_user
SET id = nextval('user_id_seq')
WHERE username = 'tacgia1';