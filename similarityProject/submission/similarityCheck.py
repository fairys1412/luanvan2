# - *-  coding: utf- 8 - *-
from sys import argv
import os
import time

#from scipy.sparse.construct import random
import tachtu
import cosinetfidf
import simR
import datetime
import pickle
from sklearn.preprocessing import LabelEncoder

# Begin đếm thời gian và load File
# start = time.time()
# fileTest = r"../Kho/test/demo EN duoc dich ra VN.txt" #r"./data/demo EN duoc dich ra VN.txt"
# fileTest = r"./data/03-AV-TRAN NGOC BICH(16-21)019.txt"
# fileTest = r"./data/16-LA-NGUYEN THI BAO ANH(131-137)047.txt"
# fileTest = r"data\Translated\S-SE\06-SE-NGUYEN THI HUYNH PHUONG(47-58)006.txt"
# End

# Begin hàm cắt kí tự , hàm đọc file
def cutFileName(fileName):
    idx = fileName.rfind('/')
    return idx+1

def openFile(filePath):
    fileOpen = open(filePath, "r", encoding='utf8')
    fileContent = fileOpen.read()
    return fileContent
# End hàm cắt kí tự , hàm đọc file

def sosanhVanBanVN():
    start = time.time()
    fileTest = r"../Kho/test/demo EN duoc dich ra VN.txt"  # r"./data/demo EN duoc dich ra VN.txt"
    # get name Paper upload
    getNameFile = tachtu.fileWordTokenize(fileTest)[0]  # thay tachtu => split
    tachtu.fileWordTokenize('../Kho/test/demo EN duoc dich ra VN.txt')
    print("Tên bài báo đang được kiểm tra: ", getNameFile.replace('TÓM TẮT', ''))
    # Begin Nhận vào tên thư mục và đặt giới hạn ngưỡng qua truyền tham số
    script, simThreshold = argv  # python similarityCheck.py <tenthumuc> <0.2>
    newSimThreshold = float(0.3)#simThreshold tham số ngưỡng
    print("Ngưỡng đang kiểm tra: {0} !".format(newSimThreshold))
    folderTopic = 'A-KHTN'  # topics[getIndex]
    print("Chủ đề của bài cần kiểm tra là: {0}".format(folderTopic))
    print("THƯ MỤC ĐANG CHẠY KINH TẾ XÃ HỘI")
    # End Dự đoán chủ đề
    # Begin xử lý theo thư mục chỉ định
    FJoin = os.path.join
    path = u'../Kho/VIE/' + folderTopic + "/"
    print("Tổng số bài báo có trong chủ đề {0} : {1}".format(folderTopic, len(os.listdir(path))))
    print('------------------------------------------------------------------------------')
    files = [FJoin(path, file) for file in os.listdir(path)]
    countPaper = 0
    for fileInDB in files:
        loadFile = fileInDB
        vector1 = cosinetfidf.text_to_vector(fileTest)
        vector2 = cosinetfidf.text_to_vector(fileInDB)
        cosineTFIDF = cosinetfidf.get_cosine(vector1, vector2)  # Tính Cosine TF-IDF
        simR_Score = simR.SIMR(fileTest, fileInDB)  # Tính vector thứ tự từ
        totalSIM = round((0.5 * cosineTFIDF) + (0.5 * simR_Score), 3)
        if totalSIM < newSimThreshold:
            pass
        else:
            countPaper += 1
            getPaperName = tachtu.fileWordTokenize(loadFile)[0]
            idx = cutFileName(fileInDB)  # Lấy index kí tự / cuối cùng
            print("File: ", fileInDB[idx:])  # Cắt index kí tự / cuối đến hết
            print("Tên bài báo: ", getPaperName)
            print("Tương đồng Cosine TF-IDF:", round(cosineTFIDF, 3))
            print("Tương đồng thứ tự từ:", round(simR_Score, 3))
            print("TF-IDF + SimR: {0}".format(totalSIM))
            print('------------------------------------------------------------------------------')
    # End xử lý theo thư mục chỉ định
    if countPaper == 0:
        print("Không có bài báo nào tương đồng với ngưỡng !")
    else:
        print("Số bài báo tương đồng với bài cần kiểm tra: {0} !".format(countPaper))
    end = time.time()
    hours, rem = divmod(end - start, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Tổng thời gian xử lý: {:0>2}h:{:0>2}m:{:5.1f}s".format(
        int(hours), int(minutes), seconds))
    return



#get name Paper upload
# getNameFile = tachtu.fileWordTokenize(fileTest)[0] #thay tachtu => split
# print("Tên bài báo đang được kiểm tra: ", getNameFile.replace('TÓM TẮT', ''))
#
# # Begin Nhận vào tên thư mục và đặt giới hạn ngưỡng qua truyền tham số
# script , simThreshold = argv #python similarityCheck.py <tenthumuc> <0.2>
# newSimThreshold = float(simThreshold)
# print("Ngưỡng đang kiểm tra: {0} !" . format(newSimThreshold))
# # End Nhận vào tên thư mục và đặt giới hạn ngưỡng qua truyền tham số
#
# # Begin Dự đoán chủ đề
# folderTopic = 'A-KHTN'#topics[getIndex]
# print("Chủ đề của bài cần kiểm tra là: {0}".format(folderTopic))
# print("THƯ MỤC ĐANG CHẠY KINH TẾ XÃ HỘI")
# # End Dự đoán chủ đề
#
# # Begin xử lý theo thư mục chỉ định
# FJoin = os.path.join
# path = u'../Kho/VIE/' + folderTopic + "/"
# print("Tổng số bài báo có trong chủ đề {0} : {1}" .format(folderTopic , len(os.listdir(path))))
# print('------------------------------------------------------------------------------')
# files = [FJoin(path, file) for file in os.listdir(path)]
# countPaper = 0
# for fileInDB in files:
#     loadFile = fileInDB
#     vector1 = cosinetfidf.text_to_vector(fileTest)
#     vector2 = cosinetfidf.text_to_vector(fileInDB)
#     cosineTFIDF = cosinetfidf.get_cosine(vector1, vector2)  # Tính Cosine TF-IDF
#     simR_Score = simR.SIMR(fileTest, fileInDB)  # Tính vector thứ tự từ
#     totalSIM = round((0.5 * cosineTFIDF) + (0.5 * simR_Score), 3)
#     if totalSIM < newSimThreshold:
#         pass
#     else:
#         countPaper += 1
#         getPaperName = tachtu.fileWordTokenize(loadFile)[0]
#         idx = cutFileName(fileInDB)  # Lấy index kí tự / cuối cùng
#         print("File: ", fileInDB[idx:])  # Cắt index kí tự / cuối đến hết
#         print("Tên bài báo: ", getPaperName)
#         print("Tương đồng Cosine TF-IDF:", round(cosineTFIDF, 3))
#         print("Tương đồng thứ tự từ:", round(simR_Score, 3))
#         print("TF-IDF + SimR: {0}" . format(totalSIM))
#         print('------------------------------------------------------------------------------')
# # End xử lý theo thư mục chỉ định
#
# if countPaper == 0:
#     print("Không có bài báo nào tương đồng với ngưỡng !")
# else:
#     print("Số bài báo tương đồng với bài cần kiểm tra: {0} !" . format(countPaper))
#
# end = time.time()
# hours, rem = divmod(end-start, 3600)
# minutes, seconds = divmod(rem, 60)
# print("Tổng thời gian xử lý: {:0>2}h:{:0>2}m:{:5.1f}s".format(
#     int(hours), int(minutes), seconds))
