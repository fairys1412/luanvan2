from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from . import views
from django.views.generic import TemplateView
# from .admin import admin_site
from django.conf import settings
from rest_framework.routers import DefaultRouter

# router = DefaultRouter()
# router.register('banthao', views.BanThaoViewSet)  # tiếp đầu ngữ
# router.register('users', views.UserViewSet)
# /banthao/ - GET
# /banthao/ - POST
# /banthao/{banthao_id} - GET
# /banthao/{banthao_id} - PUT
# /banthao/{banthao_id} - DELETE

urlpatterns = [
    # path('', include(router.urls)),
    # path('admin/', admin_site.urls)
    path('admin/', admin.site.urls),
    # template views
    # path('accounts/', include('django.contrib.auth.urls')),
    path('', views.HomeView.as_view(), name='main'),
    path('dangky/', views.SiteRegisterView.as_view(), name='dangky'),
    path('dangnhap/', views.SiteLoginView.as_view(), name='dangnhap'),
    path('hoso/', views.SiteUpdateProfileView.as_view(), name='hoso'),
    path('dangxuat/', views.SiteLogoutView.as_view(), name='dangxuat'),
    # ---- Trang gửi bản thảo
    # path('buoc1/', views.TitleView, name='buoc1'),
    # path('dstacgia/', views.AuthorList, name='dstacgia'),
    # path('buoc2/', views.AuthorView, name='buoc2'),
    # path('dstaptin/', views.FileList, name='dstaptin'),
    # path('buoc3/', views.UploadFile, name='buoc3'),
    # path('dstaptin/<int:pk>/', views.DeleteFile, name='xoafile'),   #2
    # # path('dstukhoa/', views.KeywordList, name='dstukhoa'),
    # # path('buoc4/', views.KeywordView, name='buoc4'),
    path('banthao/', views.submission_banthao, name='banthao'),
    path('tacgia/', views.submission_tacgia, name='tacgia'),
    path('taptin/', views.submission_taptin, name='taptin'),
    path('tukhoa/', views.submission_tukhoa, name='tukhoa'),
    path('tltk/', views.submission_tltk, name='tltk'),
    path('sosanhvb/', views.submission_sosanhvanban, name='sosanhvb'),
    path('sosanhvb/getDsTapTin', views.getDsTapTin, name='getDsTapTin'),
    path('sosanhvb/sosanhVanBan', views.sosanhVanBan, name='sosanhVanBan'),
    path('tacgia/post_tacgia',views.themtacgia, name='post_tacgia'),
    path('taptin/uploadfile',views.uploadfile, name='uploadfile'),
    path('banthao/post_banthao',views.thembanthao, name='post_banthao'),
    path('tukhoa/post_tukhoa', views.themtukhoa, name='post_tukhoa'),
    path('taptin/post_mabanthao',views.getTenlinhvuc, name='post_mabanthao'),
]
