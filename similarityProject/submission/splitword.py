import os
import re
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
import nltk

def split_words_text(path):
    link_take_txt = os.path.basename(path)
    if link_take_txt.endswith(".txt"):
        f = open(path, 'r+', encoding="utf-8")
        text = f.read()
        txt_string = text.split('\n')

        # print(txt_string)
        listW = []
        listU = []
        for w in txt_string:
            if w == '':
                pass
            elif w == ' ':
                pass
            elif w == '  ':
                pass
            elif w == '   ':
                pass
            else:
                listW.append(w)

        # print(listW)
        # # Only take introduction, conclusion and title page
        for b in listW:
            if b.isupper():
                listU.append(b)

        # print(listU)

        take_title = listW[2]
        rm_take_title = re.sub(r'\.|\:|\/|\*|\?|\"|\<|\>|\t', '', take_title)

        index_introduce_listU = listU[2]
        # print(index_introduce_listU)
    # ListW o day la nhung cau da duoc tach ra thanh nhung phan tu trong 1 mang
        # Lay ra index cua noi dung bien index_introduce_listU !!! 1.1INTRODUCTION
        index_introduce = listW.index(index_introduce_listU)
        # print(index_introduce)
        index_references_listU = listU[len(listU) - 1]
        index_references = listW.index(index_references_listU)
        # print(index_references)
        take_content = listW[index_introduce:index_references] #[13:199]
        # print(take_content)
        take_content_string = ' '.join(take_content)


        #preprocessor
        # print(take_content_string)
        lines = take_content_string.lower()
        # Remove tab
        rm_title_tab = re.sub(r'[\t|\/]', ' ', take_title)
        rm_content_tab = re.sub(r'\t', ' ', take_content_string)
        # print(rm_content_tab)
        # Resume content
        resume_content = rm_title_tab + " - " + rm_content_tab
        # print(resume_content)
        lines = resume_content.lower()

        # Removal of Punctuations

        chars_to_be_removed = '&#@!;,:,%,-,/,(,),<,>,*,^,~,`,.,?,|,°,=,+,_,μ,’,<=>,[,],\\,{,},±'
        rm_character = "".join(j for j in lines if j not in chars_to_be_removed)  # remove single character
        replace_number = ''
        res = re.sub(r'\d', replace_number, rm_character)
        # print(rm_character)
        rm_single_continue = [r for r in res.split() if len(r) > 1]
        # print(rm_single_continue)

        # Remove stopword
        file_stw = './data/stopword/stop_words_english.txt'
        stw_text = open(file_stw, 'r+', encoding="utf-8")
        stop_words = stw_text.read()

        word_tokens = word_tokenize(res) # res line 73

        filtered_sentence = []
        for stw in word_tokens:
            if stw not in stop_words:
                filtered_sentence.append(stw)
        # print(filtered_sentence)
        sentence_join = " ".join(filtered_sentence)
        # print(sentence_join)
        # print(sentence_join)
        # Remove stemming words
        ps = PorterStemmer()
        words = word_tokenize(sentence_join)
        stemming_words = []
        for stem_w in words:
            if stem_w not in ps.stem(stem_w):
                stemming_words.append(stem_w)

        final_words = " ".join(stemming_words)

        # Save split file
        # take_split_file = re.sub(r'\.\w+', '', link_take_txt)

        # file_new_split = open(
        #     "./../data/result/fileSplit/" + take_split_file + " - " + rm_take_title + ".txt", "w+",
        #     encoding="utf-8")

        # file_new_split.write(final_words)
        # file_new_split.close()
        # print(final_words)
        return final_words


#openFile = r"data/File convert ENG/AG/01-AG-VO QUANG MINH(1-6).txt"
# openFile = r"../Kho/ENG/ED/01-ED-LE THI HONG CAM(1-8)020.txt"
# fileProcessing = split_words_text(openFile)
# print(fileProcessing)