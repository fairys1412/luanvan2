$(function () {
    $("#luuTacGia").click(function () {
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var mail = $("#email").val();
        var maqg = $("#qg_ten").val();
        var madonvi = $("#dv_ten").val();
        $.ajax({
            url: "post_tacgia",
            type: 'POST',
            dataType: "json",
            data: {
                    'first_name': first_name,
                    'last_name': last_name,
                    'mail': mail,
                    'maqg': maqg,
                    'madonvi': madonvi
            },
            headers: {
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRFToken": getCookie("csrftoken"),
            },
            success: (data) => {
                $("#first_name").val('');
                $("#last_name").val('');
                $("#email").val('');
                $("#qg_ten").val('');
                $("#dv_ten").val('');
                jAlert(data.msg, "Thông báo");
            }
        });
    });
    $("#resetTacgia").click(function () {
        $("#first_name").val('');
        $("#last_name").val('');
        $("#email").val('');
        $("#qg_ten").val('');
        $("#dv_ten").val('');
    });
    $("#btnUploadFile").click(function () {
        var data = new FormData();
        data.append("file", $("input[id^='file_duongdan']")[0].files[0]);
        data.append("csrfmiddlewaretoken", getCookie("csrftoken"));
        data.append("ltt_ma", $("#ipLinhvuc").attr("data-id"));
        data.append("bt_ma", $("#slbanthao").val());
        data.append("ngonngu", $("#slngonngu").val());
        var slChonBT = $("#slbanthao").val();
        if (slChonBT == 0){
            jAlert("Vui lòng chọn bản thảo", "Cảnh báo");
        }else {
            $.ajax({
                url: 'uploadfile',
                type: 'POST',
                data: data,
                cache: false,
                processData: false,
                contentType: false,
                mimeType: "multipart/form-data",
                success: (data) => {
                    $('#file_duongdan').val('');
                    jAlert("Thêm thành công", "Thông báo");
                }
            });
        }
    });

    $("#resetBanThao").click(function () {
        $("#td_tieude").val('');
        $("#td_tomtat").val('');
    });

    $("#luuBanThao").click(function () {
        var tieude = $("#td_tieude").val();
        var tomtat = $("#td_tomtat").val();
        if(tieude.trim() == "" || tomtat.trim() == ""){
            jAlert("Vui lòng nhập đầy đủ thông tin", "Thông báo");
        }else{
            var lv_ma = $("#listlinhvuc").val();
            $.ajax({
            url: "post_banthao",
            type: 'POST',
            dataType: "json",
            data: {
                    'tieude': tieude,
                    'tomtat': tomtat,
                    'lv_ma': $("#listlinhvuc").val(),
            },
            headers: {
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRFToken": getCookie("csrftoken"),
            },
            success: (data) => {
                $("#td_tieude").val('');
                $("#td_tomtat").val('');
                jAlert(data.msg, "Thông báo");
            }
        });
        }
    });

    $("#btnNhaplaitu").click(function () {
        $("#tukhoa").val('');
    });

    $("#btnThemtu").click(function () {
        var tukhoa = $("#tukhoa").val();
        var idBanthao = $("#idBanthao").val();
        $.ajax({
            url: "post_tukhoa",
            type: 'POST',
            dataType: "json",
            data: {
                    'tukhoa': tukhoa,
                    'idBanthao': idBanthao,
            },
            headers: {
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRFToken": getCookie("csrftoken"),
            },
            success: (data) => {
                $("#tukhoa").val('');
                jAlert(data.msg, "Thông báo");
            }
        });
    });

    $("#slbanthao").change(function (evt) {
        $.ajax({
            url: "post_mabanthao",
            type: 'POST',
            dataType: "json",
            data: {
                    'bt_ma': $("#slbanthao").val(),
            },
            headers: {
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRFToken": getCookie("csrftoken"),
            },
            success: (data) => {
                $("#ipLinhvuc").val(data.tenlinhvuc[0].lv_ten);
                $("#ipLinhvuc").attr("data-id", data.tenlinhvuc[0].lv_ma);
            }
        });
    });

    $("#sslinhvuc").change(function (evt) {
        var linhvuc = $("#sslinhvuc").val();
        if(linhvuc == 0){
            $("#ssFile").empty();
            jAlert("Vui lòng chọn lĩnh vực", "Thông báo");
        }else{
            $("#ssFile").empty();
            $.ajax({
            url: "getDsTapTin",
            type: 'POST',
            dataType: "json",
            data: {
                    'ltt_ma': $("#sslinhvuc").val(),
            },
            headers: {
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRFToken": getCookie("csrftoken"),
            },
            success: (data) => {
                for (var i = 0; i < data.taptin.length; i++) {
                    console.log(data.taptin[i].file_duongdan)
                    var tenTaptin =  data.taptin[i].file_duongdan.split('/').pop();
                    $("#ssFile").append(new Option(tenTaptin, data.taptin[i].file_duongdan));
                }
            }
        });

        }
    });

    $("#btnSosanh").click(function () {
        var linhvuc = $("#sslinhvuc").val();
        if(linhvuc == 0){
            jAlert("Vui lòng chọn lĩnh vực", "Thông báo");
        }else{
            document.body.className = "loading";
                $.ajax({
                url: "sosanhVanBan",
                type: 'POST',
                dataType: "json",
                data: {
                        'ltt_ma': $("#sslinhvuc").val(),
                        'ssngonngu': $("#ssngonngu").val(),
                        'nguongSS': $("#txtNguongSS").val(),
                        'fileUrl': $("#ssFile").val(),
                        'kholinhvuc': $("#sslinhvuc").val()
                },
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRFToken": getCookie("csrftoken"),
                },
                success: (data) => {
                    console.log(data.data)
                    document.body.className = "";
                    $("#txtKetqua").val(data.data);
                }
            });
        }
    });
});

function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    const cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + "=")) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}