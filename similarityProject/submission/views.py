# - *-  coding: utf- 8 - *-
import nltk.tokenize
import underthesea
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import TemplateView, FormView, UpdateView, ListView, CreateView, DetailView
from django.views.generic import ListView, TemplateView
from django.db import connection
from django.http import JsonResponse
from .models import *
from django.conf import settings

from collections import Counter
from nltk.stem import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from datetime import datetime
import pandas as pd
import math
from sys import argv
import os
import time
from underthesea import word_tokenize
from underthesea import sent_tokenize
from nltk.tokenize import word_tokenize
import regex as re
import docx2txt
from pygoogletranslation import Translator
# from .serializers import BanThaoSerializer, TLTKSerializer, UserSerializer

env = '/Users/admin/Documents/nienluan-luanvan/djangoProject/similarityProject'#'/Users/macos/Canhan/ThuChi/luanvan2/similarityProject'
env2 = '/Users/admin/Documents/nienluan-luanvan/djangoProject/similarityProject/submission/'#'/Users/macos/Canhan/ThuChi/luanvan2/similarityProject/submission/'
envSaveFiletxt = '/Users/admin/Documents/nienluan-luanvan/djangoProject/similarityProject/submission/static/files/'#'/Users/macos/Canhan/ThuChi/luanvan2/similarityProject/submission/static/files/'
# [STOPWORD_BEGIN]
fileName3 = env2 + "VI_StopWord.txt"
file_input2 = open(fileName3, "r+", encoding="utf-8")
read_file2 = file_input2.read()
stopword = read_file2.split("\n")
# [STOPWORD_END]
negative_word =  ['không', 'khỏi', 'chẳng', 'đâu', 'chưa']
data2 = pd.read_excel(env2 + "Vietnamese_SYN_ANT.xlsx")

def check_negative_word(vector):
  for word in vector:
    if word in negative_word:
      return True
  return False

def eliminate_stop_negative_word(vec1_del,vec2_del):
  for i in vec1_del:
    if i in stopword and i not in negative_word:
      dela = vec1_del.pop(vec1_del.index(i))
  for i in vec2_del:
    if i in stopword and i not in negative_word:
      delb = vec2_del.pop(vec2_del.index(i))
  for i in vec1_del:
    for j in vec2_del:
      if (i in negative_word and j in negative_word):
        dela = vec1_del.pop(vec1_del.index(i))
        delb = vec2_del.pop(vec2_del.index(j))
        break
  return vec1_del, vec2_del


def important_pos_tag(text):
    pos_tag_temp = underthesea.pos_tag(text)
    final = []
    for i in pos_tag_temp:
      if i[1] == 'N' or i[1] == 'V' or i[1] == 'A' or i[1] == 'Np' or (i[1] == 'R' and i[0] == 'không') or i[1] == 'M' or i[1] == 'Nu' or i[1] == 'Ny' or i[1] == 'M' or i[1] == 'V' or i[1] == 'A' or i[1] in negative_word:
        final.append(i[0])
    if len(final) != 0:
        return final

def check_semantic(firstSentence, secondSentence):
    sentence_1 = important_pos_tag(firstSentence)
    # thu_tu_1 = generate_order(firstSentence)
    sentence_1_lower = []
    for i in sentence_1:
        sentence_1_lower.append(i.lower())

    sentence_2 = important_pos_tag(secondSentence)
    # thu_tu_2 = generate_order(secondSentence)
    sentence_2_lower = []
    for i in sentence_2:
        sentence_2_lower.append(i.lower())

    sentence_1_del, sentence_2_del = eliminate_stop_negative_word(sentence_1_lower, sentence_2_lower)

    # sentence_1_del, sentence_2_del = (sentence_1_lower, sentence_2_lower) #tạm ngưng tính năng bỏ stopword
    intersection = set(sentence_1_lower).intersection(set(sentence_2_lower))
    difference_1, difference_2 = list(set(sentence_1_del).difference(intersection)), list(
        set(sentence_2_del).difference(intersection))

    negativeCheck = check_negative_word(difference_1) ^ check_negative_word(difference_2)

    count_semantic = []

    sum_different = difference_1 + difference_2

    if len(intersection) != 0:
        if ((len(difference_1) - len(intersection) > 0) and (len(difference_2) - len(intersection) > 0)):
            return False
        else:
            if len(difference_1) != 0 and len(difference_2) != 0:
                for word_1 in difference_1:
                    for word_2 in difference_2:
                        check_meaning = data2['First'].isin([word_1]) & data2['Second'].isin([word_2])
                        if 'SYN' in data2[check_meaning].Relation.values:
                            if negativeCheck == False:
                                if word_1 not in count_semantic:
                                    count_semantic.append(word_1)
                                if word_2 not in count_semantic:
                                    count_semantic.append(word_2)

                        elif 'ANT' in data2[check_meaning].Relation.values:
                            if negativeCheck == True:
                                if word_1 not in count_semantic:
                                    count_semantic.append(word_1)
                                if word_2 not in count_semantic:
                                    count_semantic.append(word_2)
                if len(count_semantic) != 0:
                    return True
                else:
                    return False
            elif ((len(difference_1) != 0 and len(difference_2) == 0) or (
                    len(difference_1) == 0 and len(difference_2) != 0)):
                return False
            else:
                return True
    else:
        return False

# def check_semantic(vector1, vector2):
#     # xóa từ phủ định ngay khi tách từ, với từng cặp từ phủ định ở 2 câu mới được xóa. sau đó mới tìm tập từ chung riêng.
#     vec1, vec2 = word_tokenize(vector1.lower()), word_tokenize(vector2.lower())  # chuyển sang in thường rồi tách từ
#     vec1_del, vec2_del = eliminate_stop_negative_word(vec1.copy(), vec2.copy())  # xóa từ dừng trong các tập riêng
#     intersection = set(vec1).intersection(set(vec2))
#     difference_vec1, difference_vec2 = list(set(vec1_del).difference(intersection)), list(
#         set(vec2_del).difference(intersection))
#     # if len(intersection) == 0:
#     #   print("  - tập từ chung: không có")
#     # else:
#     #   # print("  - tập từ chung: ", intersection_final)
#     #   print("  - tập từ chung: ", intersection)
#     # print("  - tập từ riêng vector1: ", difference_vec1)
#     # print("  - tập từ riêng vector2: ", difference_vec2)
#     ktra = check_negative_word(difference_vec1) ^ check_negative_word(difference_vec2)
#     # print(check_negative_word(difference_vec1))
#     # print(check_negative_word(difference_vec2))
#     # print(ktra)
#     if len(intersection) != 0:
#         if len(difference_vec1) != 0 and len(difference_vec2) != 0:
#             for i in difference_vec1:
#                 for j in difference_vec2:
#                     # num = data[data['First'].isin([difference_vec1[i], difference_vec2[j]]) & data['Second'].isin([difference_vec1[i], difference_vec2[j]])]
#                     new = data2['Second'].isin([i]) & data2['First'].isin([j])
#                     new_2 = data2['First'].isin([i]) & data2['Second'].isin([j])
#                     # print(check_negative_word(difference_vec1)," - ", check_negative_word(difference_vec2)," : ",ktra)
#                     if ('SYN' in data2[new].Relation.values or 'SYN' in data2[new_2].Relation.values):  # Đồng nghĩa
#                         # print(i," - ", j, ": đồng nghĩa")
#                         if ktra == False:
#                             return True
#                         else:
#                             return False
#
#                     elif ('ANT' in data2[new].Relation.values or 'ANT' in data2[new_2].Relation.values):  # Trái nghĩa
#                         # print(i," - ", j, ": trái nghĩa")
#                         if (ktra == True):  # True
#                             return True
#                         else:
#                             return False
#                     else:
#                         return False
#         elif (len(difference_vec1) == 0 or len(difference_vec2) == 0):
#             if negative_word in difference_vec1 or negative_word in difference_vec2:
#                 return False
#             else:
#                 return True
#         else:
#             return False
#     else:
#         return False

def Sum(test_content): # đếm số lượng word
    tong = len(re.findall(r'\w+', test_content))
    return tong

def Tachcau(text):
    ketqua = sent_tokenize(text)
    return ketqua

def TachTu(text):
    dau = [':', '.', ',', '/', '{', '}', '(', ')', '!', '#', '@', '$', '%', '^', '&', '*', '-', '_', '+', '=', ';', '?', '~', '`', '|']
    ketqua = []
    for word in text:
        temp = word_tokenize(word)
        for i in temp:
            if (i in dau):
                temp.remove(i)
            else:
                ketqua.append(i)
    return Counter(ketqua)

def DOIcheck(text):  # Kiểm tra mã DOI của tài liệu
    result = re.findall(r'DOI:[ ]{0,}\b(10[.][0-9]{4,}(?:[.][0-9]+)*[ ]{0,}/[ ]{0,}(?:(?!["&\'<>])\S)+)\b', text)
    if len(result) != 0:
        return True
    else:
        return False


def listToString(s):
    str1 = " "
    return (str1.join(s))


def stringToList(string):
    li = list(string.split(" "))
    return li

def fileWordTokenize2(fileName):
    fileInput = open(fileName, "r+", encoding="utf-8")
    readFile = fileInput.read()
    splitFileWithLines = readFile.split("\n")  # chia file thành các dòng ứng với từng phần tử trong LIST
    for sentences in splitFileWithLines:  # Xoá các khoảng trắng thừa trong các câu
        splitFileWithLines[splitFileWithLines.index(sentences)] = " ".join(sentences.split())
    for number in range(len(splitFileWithLines)):  # Dùng để thay thế TRỪU TƯỢNG thành TÓM TẮT
        if splitFileWithLines[number] == 'TRỪU TƯỢNG':  #
            splitFileWithLines[number] = 'TÓM TẮT'
        if "..." in splitFileWithLines[number]:
            splitFileWithLines[number] = splitFileWithLines[number].replace("...", "")
    abstract_index = 0
    abstract_vn_index = 0
    start_index = 0
    end_index = 0
    for sentences in splitFileWithLines:
        if sentences == 'ABSTRACT':
            abstract_index = splitFileWithLines.index(sentences)
        if sentences == 'TÓM TẮT':
            abstract_vn_index = splitFileWithLines.index(sentences)
        if sentences == 'TÀI LIỆU THAM KHẢO':
            end_index = splitFileWithLines.index(sentences)
    if abstract_index < abstract_vn_index:
        start_index = abstract_index
    else:
        start_index = abstract_vn_index
    splitFileWithLines_after = []
    for number in range(start_index + 1, end_index):
        splitFileWithLines_after.append(splitFileWithLines[number])
    TITTLE_re = """(^\d |)[A-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỰ \-'"]{1,}"""
    tittle_re = """^\d{1,}[.]{1,}\d{1,}[.]{0,}\d{0,}[.]{0,}\d{0,}[.]{0,}\s{0,}[-a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂưăạảấầẩẫậắằẳẵặẹẻẽếềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ, 0123456789\-'"\[\]]{1,}"""
    pic_table_re = """(^Hình |^Bảng )[0-9][a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂưăạảấầẩẫậắằẳẵặẹẻẽếềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ,':/{}()!#@$%^&*_+=;?~`| 0123456789\-'"\[\]]{1,}"""
    step_re = """(^Bước )[0-9][a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂưăạảấầẩẫậắằẳẵặẹẻẽếềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ,':/{}()!#@$%^&*_+=;?~`| 0123456789\-'"\[\].]{1,}"""
    number_re = """(^\d|)+[\W\d_]{0,}"""
    splitFile = []
    for sentences in splitFileWithLines_after:  # Bỏ các tiêu đề, bảng và hình
        checktittle = re.sub(tittle_re, '', sentences)
        checkTITTLE = re.sub(TITTLE_re, '', sentences)
        checkpic_table = re.sub(pic_table_re, '', sentences)
        checkstep = re.sub(step_re, '', sentences)
        checknumber = re.sub(number_re, '', sentences)
        if len(checktittle) != 0 and len(checkTITTLE) != 0 and len(checkpic_table) != 0 and len(checkstep) != 0 and len(
                checknumber) != 0:
            splitFile.append(sentences)
    join_splitFile = listToString(splitFile)
    final_fileWordTokenize = join_splitFile.split(". ")
    return final_fileWordTokenize

#end ngữ nghĩa



class HomeView(TemplateView):
    template_name = 'home.html'


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email']


class SiteRegisterView(FormView):
    template_name = 'dangky.html'
    form_class = RegisterForm

    # Xử lý dữ liệu từ filed. Sau khi get data, return về 1 trang web mà ta mong muốn và lưu xuống CSDL
    def form_valid(self, form):
        data = form.cleaned_data
        new_user = User.objects.create_user(
            username=data['username'],
            password=data['password1'],
            email=data['email']
        )
        url = f"{reverse('dangnhap')}?username={new_user.username}"
        from pprint import pprint;
        pprint(url)
        # Cho phép chuyển sang 1 trang khác
        return redirect(url)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['username'] = self.request.GET.get('username')
        return context


class SiteLoginView(LoginView):
    template_name = 'dangnhap.html'


class SiteUpdateProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'profile.html'
    model = User
    fields = ['anhdaidien', 'first_name', 'last_name', 'email']
    success_url = '/hoso/'

    def get_object(self, queryset=None):
        return self.request.user


class SiteLogoutView(LogoutView):
    template_name = 'dangxuat.html'


def dictfetchall(cursor):
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def submission_banthao(request):
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM submission_linhvuc")
        linhvuc = dictfetchall(cursor)
    finally:
        cursor.close()
    return render(request, 'submission_banthao.html', {'linhvuc': linhvuc})


def submission_tacgia(request):
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM submission_quocgia")
        quocgia = dictfetchall(cursor)
        cursor.execute("SELECT * FROM submission_donvi")
        donvi = dictfetchall(cursor)
    finally:
        cursor.close()
    return render(request, 'submission_tacgia.html', {'quocgia': quocgia, 'donvi': donvi})

def submission_taptin(request):
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM submission_loaitaptin")
        loaitaptin = dictfetchall(cursor)
        cursor.execute("SELECT * FROM submission_banthao")
        banthao = dictfetchall(cursor)
    finally:
        cursor.close()
    return render(request, 'submission_taptin.html',{'loaitaptin': loaitaptin, 'banthao': banthao})

def submission_tukhoa(request):
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM submission_banthao")
        banthao = dictfetchall(cursor)
    finally:
        cursor.close()
    return render(request, 'submission_tukhoa.html',{'banthao': banthao})

def submission_tltk(request):
    return render(request, 'submission_tltk.html')

def submission_sosanhvanban(request):
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM submission_linhvuc")
        linhvuc = dictfetchall(cursor)
    finally:
        cursor.close()
    return render(request, 'submission_sosanhvanban.html',{'linhvuc': linhvuc})

# Phương thức GET
def GetItemQuocGia(request):
    qg_ma = request.GET.get('qg_ma', None)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM submission_quocgia WHERE qg_ma=" + qg_ma)
    quocgia = cursor
    return JsonResponse(quocgia)

def getDsTapTin(request):
    ltt_ma = request.POST.get('ltt_ma', None)
    cursor = connection.cursor()
    print('ltt_ma:', ltt_ma)
    cursor.execute("SELECT * FROM submission_taptin WHERE ltt_ma=" + ltt_ma)
    taptin = dictfetchall(cursor)
    return JsonResponse({'taptin': taptin})


#Phuong thức POST
def themtacgia(request):
    first_name = request.POST.get('first_name')
    last_name = request.POST.get('last_name')
    mail = request.POST.get('mail')
    maqg = request.POST.get('maqg')
    madonvi = request.POST.get('madonvi')
    try:
        cursor = connection.cursor()
        cursor.execute("INSERT INTO submission_user (is_superuser, first_name, last_name, email, is_staff, is_active,gioitinh, maqg, madonvi) "
                       "VALUES (true, '"+ first_name +"', '"+ last_name +"', '"+ mail +"', true, true, 'male', '"+ maqg +"', '"+ madonvi +"')")
    finally:
        cursor.close()
    return JsonResponse({'msg': 'Thêm thành công'})

def uploadfile(request):
    file = request.FILES.get("file")
    ltt_ma = request.POST.get("ltt_ma")
    bt_ma = request.POST.get("bt_ma")
    ngonngu = request.POST.get("ngonngu")
    print(ltt_ma)
    fs = FileSystemStorage()
    filename = fs.save(settings.MEDIA_ROOT + '/files/' + file.name, file)
    path = settings.MEDIA_ROOT + filename
    print(path)
    try:
        cursor = connection.cursor()
        cursor.execute("INSERT INTO submission_taptin(file_duongdan, ltt_ma, bt_ma, ngonngu) VALUES ('"+ path +"', "+ ltt_ma +","+ bt_ma +","+ ngonngu +")")
    finally:
        cursor.close()
    return JsonResponse({'msg': 'Thêm thành công'})

def thembanthao(request):
    tieude = request.POST.get('tieude')
    tomtat = request.POST.get('tomtat')
    lv_ma = request.POST.get('lv_ma')
    try:
        cursor = connection.cursor()
        cursor.execute("INSERT INTO submission_banthao(bt_tieude, bt_tomtat, bt_ngaytao, lv_ma) "
                       "VALUES ('"+ tieude +"', '"+ tomtat +"', NOW(), "+ lv_ma +")")
    finally:
        cursor.close()
    return JsonResponse({'msg': 'Thêm thành công'})

def themtukhoa(request):
    tukhoa = request.POST.get('tukhoa')
    bt_ma = request.POST.get('idBanthao')
    try:
        cursor = connection.cursor()
        cursor.execute("INSERT INTO submission_tukhoa(tukhoa, bt_ma, nn_ma) "
                       "VALUES ('"+ tukhoa +"', "+ bt_ma +", 1)")
    finally:
        cursor.close()
    return JsonResponse({'msg': 'Thêm thành công'})

def getTenlinhvuc(request):
    bt_ma = request.POST.get('bt_ma')
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT b.lv_ten, b.lv_ma FROM submission_banthao a, submission_linhvuc b WHERE "
                       "a.lv_ma = b.lv_ma AND a.bt_ma = " + bt_ma)
        tenlinhvuc = dictfetchall(cursor)
    finally:
        cursor.close()
    return JsonResponse({'tenlinhvuc': tenlinhvuc})

def sosanhVanBanTiengViet(request):
    lv_ma = request.POST.get('sslinhvuc')
    ngonngu = request.POST.get('ssngonngu')
    maTaptin = request.POST.get('ssFile')

def sosanhVanBan(request):
    ssngonngu = request.POST.get('ssngonngu')
    nguongSS = request.POST.get('nguongSS')
    fileUrl = request.POST.get('fileUrl')
    kholinhvuc = request.POST.get('kholinhvuc')
    print('fileUrl:',fileUrl)
    text = docx2txt.process(fileUrl)
    translator = Translator()
    with open(envSaveFiletxt + "Output.txt", "w") as text_file:#tiếng việt
        print(text, file=text_file)
    # txtEN = translator.bulktranslate(envSaveFiletxt + "Output.txt", src="vi", dest="en")
    # print('txtEN.text:', txtEN.text)
    # print('txtEN:', txtEN)
    # with open(envSaveFiletxt + "OutputEn.txt", "w") as text_file:#tiếng anh
    #     print(txtEN.text, file=text_file)
    if ssngonngu == '2':#tieng viet
        str = sosanhVanBanVN(envSaveFiletxt + "Output.txt",text,nguongSS, kholinhvuc)
    else:#tieng anh
        str = sosanhVanBanEN(envSaveFiletxt + "OutputEn.txt", text, nguongSS)
    return JsonResponse({'data': str})


#simR
def xoa_trung_nhau(fileTxt):
    file_input = open(fileTxt, "r+", encoding="utf-8")
    read_file = file_input.read()
    # Phân tách dữ liệu nhận được từ kiểu String chuyển sang danh sách List
    words = read_file.split(' ')
    count_each_word = Counter(words)
    # Loại bỏ các từ lặp lại trong file Text
    del_word = dict(count_each_word)
    return list(del_word.keys())


# ---------------Tập từ phân biệt của mỗi file ---------------
def tap_tu_phan_biet(fileText_1, fileText_2):
    tap_tu = []
    # Hợp hai tập fileText lại với nhau
    # fileText_1.extend(fileText_2)
    phepHop = fileText_1 + fileText_2
    # Loại bỏ từ trùng nhau
    for i in phepHop:
        if i in tap_tu:
            pass
        else:
            tap_tu.append(i)
    return tap_tu


# # ---------------Vector thứ tự từ ---------------
def vector_thu_tu_tu(tap_tu, file):
    vector = []
    vitri = 0
    for i in range(len(tap_tu)):
        for j in range(len(file)):
            if tap_tu[i] == file[j]:
                vitri = j + 1
                vector.append(j)
            if vitri == 0 and j == (len(file) - 1):
                vitri = 0
                vector.append(vitri)
        vitri = 0
    return vector


def SIMR(file_1, file_2):
    # Đọc và xóa các phần tử (từ) trùng nhau của cả 2 File
    temp1 = xoa_trung_nhau(file_1)
    temp2 = xoa_trung_nhau(file_2)
    # print("temp1: " + str(temp1))
    # print("temp2: " + str(temp2))
    # Hợp 2 list (list temp1 và list temp2) lại với nhau để tạo ra "tập từ phân biệt"
    tap_tu = tap_tu_phan_biet(temp1, temp2)
    # print("-----------Tập từ Phân biệt: \n" + str(tap_tu))
    # Vector thứ tự từ
    vector_1 = vector_thu_tu_tu(tap_tu, temp1)
    vector_2 = vector_thu_tu_tu(tap_tu, temp2)
    # print("Vector thứ tự từ 1: " + str(vector_1))
    # print("Vector thứ tự từ 2: " + str(vector_2))
    # print(len(temp1))
    # print(len(temp2))
    # print(len(tap_tu))
    # print(len(vector_1))
    # print(len(vector_2))
    # Hiệu số simR
    hieuSo = 0
    if len(vector_1) == len(vector_2):
        for i in range(len(vector_1)):
            hieuSo += (vector_1[i] - vector_2[i]) * (vector_1[i] - vector_2[i])
    # Căn bậc 2 của hiệu số
    canBac_hieuSo = math.sqrt(hieuSo)
    # Tổng số simR
    tongSo = 0
    if len(vector_1) == len(vector_2):
        for i in range(len(vector_1)):
            tongSo += (vector_1[i] + vector_2[i]) * (vector_1[i] + vector_2[i])
    # Căn bậc 2 của hiệu số
    canBac_tongSo = math.sqrt(tongSo)
    # Tính simR (độ tương đồng thứ tự từ)
    simR = (1 - (canBac_hieuSo / canBac_tongSo))
    # print("Độ tường đồng thứ tự từ: ", simR)
    return simR

#end simR

#splitword
def split_words_text(path):
    link_take_txt = os.path.basename(path)
    if link_take_txt.endswith(".txt"):
        f = open(path, 'r+', encoding="utf-8")
        text = f.read()
        txt_string = text.split('\n')
        # print(txt_string)
        listW = []
        listU = []
        for w in txt_string:
            if w == '':
                pass
            elif w == ' ':
                pass
            elif w == '  ':
                pass
            elif w == '   ':
                pass
            else:
                listW.append(w)
        # print(listW)
        # # Only take introduction, conclusion and title page
        for b in listW:
            if b.isupper():
                listU.append(b)
        # print(listU)
        take_title = listW[2]
        rm_take_title = re.sub(r'\.|\:|\/|\*|\?|\"|\<|\>|\t', '', take_title)
        index_introduce_listU = listU[2]
        # print(index_introduce_listU)
        # ListW o day la nhung cau da duoc tach ra thanh nhung phan tu trong 1 mang
        # Lay ra index cua noi dung bien index_introduce_listU !!! 1.1INTRODUCTION
        index_introduce = listW.index(index_introduce_listU)
        # print(index_introduce)
        index_references_listU = listU[len(listU) - 1]
        index_references = listW.index(index_references_listU)
        # print(index_references)
        take_content = listW[index_introduce:index_references] #[13:199]
        # print(take_content)
        take_content_string = ' '.join(take_content)
        #preprocessor
        # print(take_content_string)
        lines = take_content_string.lower()
        # Remove tab
        rm_title_tab = re.sub(r'[\t|\/]', ' ', take_title)
        rm_content_tab = re.sub(r'\t', ' ', take_content_string)
        # print(rm_content_tab)
        # Resume content
        resume_content = rm_title_tab + " - " + rm_content_tab
        # print(resume_content)
        lines = resume_content.lower()
        # Removal of Punctuations
        chars_to_be_removed = '&#@!;,:,%,-,/,(,),<,>,*,^,~,`,.,?,|,°,=,+,_,μ,’,<=>,[,],\\,{,},±'
        rm_character = "".join(j for j in lines if j not in chars_to_be_removed)  # remove single character
        replace_number = ''
        res = re.sub(r'\d', replace_number, rm_character)
        # print(rm_character)
        rm_single_continue = [r for r in res.split() if len(r) > 1]
        # print(rm_single_continue)
        # Remove stopword
        file_stw = env + '/Kho/stopword/stop_words_english.txt'#'./data/stopword/stop_words_english.txt'
        stw_text = open(file_stw, 'r+', encoding="utf-8")
        stop_words = stw_text.read()
        word_tokens = nltk.tokenize.word_tokenize(res)#word_tokenize(res) # res line 73
        filtered_sentence = []
        for stw in word_tokens:
            if stw not in stop_words:
                filtered_sentence.append(stw)
        # print(filtered_sentence)
        sentence_join = " ".join(filtered_sentence)
        # print(sentence_join)
        # print(sentence_join)
        # Remove stemming words
        ps = PorterStemmer()
        words = nltk.tokenize.word_tokenize(sentence_join)
        stemming_words = []
        for stem_w in words:
            if stem_w not in ps.stem(stem_w):
                stemming_words.append(stem_w)
        # print('stemming_words:', stemming_words)
        final_words = " ".join(stemming_words)
        # Save split file
        # take_split_file = re.sub(r'\.\w+', '', link_take_txt)
        # file_new_split = open(
        #     "./../data/result/fileSplit/" + take_split_file + " - " + rm_take_title + ".txt", "w+",
        #     encoding="utf-8")
        # file_new_split.write(final_words)
        # file_new_split.close()
        # print('final_words:', final_words)
        return final_words

#end splitword


#cosinetfidf
def get_cosine(vec1, vec2):
    # Giao vec1 và vec2 để Tập hợp các phần tử thuộc cả vec1 và vec2
    intersection = set(vec1.keys()) & set(vec2.keys())
    # Tử số (lấy số lần xuất hiện của 1 từ mà từ đó xuất hiện ở cả vec1 và vec2 nhân với nhau, lấy tổng số)
    numerator = sum([vec1[x] * vec2[x] for x in intersection])
    sum1 = sum([vec1[x] ** 2 for x in list(vec1.keys())])
    sum2 = sum([vec2[x] ** 2 for x in list(vec2.keys())])
    # Mẫu số
    denominator = math.sqrt(sum1) * math.sqrt(sum2)

    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator


def text_to_vector(filePath):
    # print(filePath)
    content = open(filePath, encoding='utf-8')
    contentFile = content.read()
    # contentFile = tachtu.fileWordTokenize(filePath)[1] # Tách từ cho tiếng Việt.
    contentFile = split_words_text(filePath) # Tách từ cho tiếng Anh.
    # print('\n', contentFile)
    # Khởi tạo đối tượng vector
    tfidfvectorizer = TfidfVectorizer(analyzer='word')
    tfidf_wm = tfidfvectorizer.fit_transform([contentFile])
    tfidf_tokens = tfidfvectorizer.get_feature_names()
    df_tfidfvect = pd.DataFrame(data=tfidf_wm.toarray(), columns=tfidf_tokens)
    dictR = df_tfidfvect.to_dict(orient='index')
    dictKeys = dictR.keys()
    dictRV = {}
    for x in dictKeys:
        dictRV = dict(dictR[x])
    return dictRV


#end cosinetfidf

#tachtu.py

def fileWordTokenize(fileName):
    print('fileName:', fileName)
    file_input = open(fileName, "r+", encoding="utf-8")
    read_file = file_input.read()  # Đọc nội dung của File
    # Tách nội dung File theo từng dòng vô list
    list_string = read_file.split('\n')
    listWord = []  # Đưa vào list mới sau khi xử lý xóa các kí tự không cần thiết và bỏ DOI
    listUpper = []  # Lấy ra những tiêu đề viết hoa
    for sen1 in range(len(list_string)):
        if list_string[sen1] == 'TRỪU TƯỢNG':
            list_string[sen1] = 'TÓM TẮT'
    # Đọc từng câu trong list ban đầu và xóa 1 vài chỗ ko cần thiết, cho vào listW
    for sen in list_string:
        subText = re.sub(r'\t|^\s+|\s+$|\ufeff', '', sen)
        i = subText
        if i == '' or 'DOI:' in i:
            pass
        else:
            listWord.append(i)
    # Begin Lấy ra lời cảm tạ thì xóa từ đó trở xuống
    flag = 0  # Mark nếu = 1 là có LCT và đã xóa rồi, xóa bao gồm TLTK
    for index, value in enumerate(listWord):
        if value == 'LỜI CẢM TẠ' or value == 'LỜI CẢM ƠN':
            indexLCT = index
            del listWord[indexLCT:]
            flag = 1
    # End Lấy ra index tài liệu tham khảo và xóa từ đó trở xuống
   # Begin Kiểm tra đề mục nào viết hóa thì đưa vào listUpper
    for senW in range(len(listWord)):
        if listWord[senW].isupper():
            listUpper.append(listWord[senW])
    for senU in range(len(listUpper)):
        if listUpper[senU] == 'TÓM TẮT':
            listUpper[senU] = 'TÓM TẮT'
    # End Kiểm tra đề mục nào viết hóa thì đưa vào listUpper
    # Begin lấy ra và xử lý và lấy ra tiêu đề
    print('listUpper:', listUpper)
    title_index_listUpper = listUpper.index('TÓM TẮT')
    if title_index_listUpper == 2:
        get_title = listWord[0]
    elif title_index_listUpper == 4:
        title_list = listUpper[0:3]
        get_title = ' '.join(title_list)
    else:
        title_list = listUpper[0:2]
        get_title = ' '.join(title_list)
    title = get_title  # Lấy ra tên tiêu đề bài báo
    # End lấy ra và xử lý tiêu đề
    # Begin Lấy nội dung từ Giới thiệu đến Kết luận
    tomtat_listUpper = listUpper.index('TÓM TẮT')
    print('tomtat_listUpper:', tomtat_listUpper)
    gioithieu_listUpper = listUpper[tomtat_listUpper + 1]
    gioiThieu = listWord.index(gioithieu_listUpper)
    # Lấy ra tài liệu tham khảo nếu ko có LCT
    if flag == 1:
        content = listWord[gioiThieu:]
    else:
        tailieu_listUpper = listUpper[len(listUpper) - 1]
        tailieu = listWord.index(tailieu_listUpper)
        content = listWord[gioiThieu:tailieu]
    # Begin Kết hợp từ Giới thiệu đến Kết luận
    contentJoin = ' '.join(content)  # Ghép lại thành 1 text duy nhất từ list
    contentSplit = contentJoin.split(' ')
    # End Kết hợp từ Giới thiệu đến Kết luận
    # Begin Xóa đi số và công thức
    content_math = []
    for w in contentSplit:
        re_w_one = re.sub(
            r'[\d,():=/.^°\-_¬∧∨∃∅&@+%;■*Ωλπω∈βαηρ〖〗⃗\[\]⁡{}∑▒█φθ ̂>γ&#@<!?~`´⊆<=>±→∅–"┤├│∀≤≥|√ŷ∩∪×\'‖┬∏ε¯⁻−↔ξ⊂μ⋯δïф∇∙∫ϱ↦∞ωψ∞∆∬σ″²⟦⟧∙∂≠≔⋅ζ ⁄ç•≮∉\⇔∃ º⌉⌈〉〈□]', ' ', w)
        re_w_two = re.sub(r'(m)s|\_\w+|sin|cos|khz|^\s+$', '', re_w_one)
        if re_w_two == '' or re_w_two == ' ' or len(re_w_two) <= 2:
            pass
        else:
            content_math.append(re_w_two)
    # End Xóa đi số và công thức
    # Begin loại bỏ tiếp các phần còn thừa
    content_string_two = ' '.join(content_math)
    word_split = content_string_two.split(' ')
    listWord_split = []
    for word in word_split:
        re_w_two = re.sub(r'(m)s|\_\w+|sin|cos|khz|^\s+$', '', word)
        if re_w_two == '' or re_w_two == ' ' or len(re_w_two) <= 2:
            pass
        else:
            listWord_split.append(word)
    # End loại bỏ tiếp các phần còn thừa
    # Begin Đưa hết về lower
    content_string_three = ' '.join(filter(str.isalpha, listWord_split))
    content_lower = content_string_three.lower()
    # End Đưa hết về lower
    # Begin tách từ
    content_process = underthesea.word_tokenize(content_lower, format="text")
    content_process_split = content_process.split(' ')
    # End tách từ
    # Begin Xóa Stopword
    stopwords = open(env + "/Kho/stopword/stopwords.txt",
                     "r+", encoding="utf-8")
    stopwords_read = stopwords.read()
    stopwords_split = stopwords_read.split('\n')
    content_stopword = []
    for n in content_process_split:
        if n in stopwords_split:
            pass
        else:
            content_stopword.append(n)
    # End Xóa Stopword
    # Begin Final content
    contentFinal = ' '.join(content_stopword)
    # End Final content
    return title, contentFinal

#end tachtu.py



#similarityCheck
def cutFileName(fileName):
    idx = fileName.rfind('/')
    return idx+1

def openFile(filePath):
    fileOpen = open(filePath, "r", encoding='utf8')
    fileContent = fileOpen.read()
    return fileContent
# End hàm cắt kí tự , hàm đọc file

def getNamePaperEN(fileName):
    file_input = open(fileName, "r+", encoding="utf-8")
    read_file = file_input.read()  # Đọc nội dung của File
    list_string = read_file.split('\n')
    print('list_string:', list_string)
    print('list_string[0]:', list_string[1])
    return list_string[1]

def getNamePaperVN(fileName):
    file_input = open(fileName, "r+", encoding="utf-8")
    read_file = file_input.read()  # Đọc nội dung của File
    list_string = read_file.split('\n')
    print('list_string:', list_string)
    print('list_string[0]:', list_string[12])
    return list_string[12]


def sosanhVanBanEN(filetxt, text, nguongSS):
    str = ""
    start = time.time()
    fileTest = r''+filetxt#r"/Users/macos/Canhan/ThuChi/luanvan2/similarityProject/Kho/test/demo EN duoc dich ra VN.txt"  # r"./data/demo EN duoc dich ra VN.txt"
    # get name Paper upload
    getNamePaperEN(fileTest)
    getNameFile = split_words_text(fileTest)  # thay tachtu => split
    print("Tên bài báo đang được kiểm tra: ", getNameFile.replace('Abstract', ''))
    str += "Tên bài báo đang được kiểm tra: " + getNamePaperEN(fileTest) + "\n"
    # Begin Nhận vào tên thư mục và đặt giới hạn ngưỡng qua truyền tham số
    script, simThreshold = argv  # python similarityCheck.py <tenthumuc> <0.2>
    newSimThreshold = float(nguongSS)#simThreshold
    print("Ngưỡng đang kiểm tra: {0} !".format(newSimThreshold))
    str += "Ngưỡng đang kiểm tra: {0} !".format(newSimThreshold) + "\n";
    folderTopic = 'B-CN'  # topics[getIndex]
    print("Chủ đề của bài cần kiểm tra là: {0}".format(folderTopic))
    str += "Chủ đề của bài cần kiểm tra là: {0}".format(folderTopic) + "\n"
    print("CHĂN NUÔI VÀ THÚ Y")
    str += "CHĂN NUÔI VÀ THÚ Y" + "\n"
    # End Dự đoán chủ đề
    # Begin xử lý theo thư mục chỉ định
    FJoin = os.path.join
    path = u''+ env + '/Kho/ENG/' + folderTopic + "/"
    print("Tổng số bài báo có trong chủ đề {0} : {1}".format(folderTopic, len(os.listdir(path))))
    str += "Tổng số bài báo có trong chủ đề {0} : {1}".format(folderTopic, len(os.listdir(path))) + "\n"
    print('------------------------------------------------------------------------------')
    str += '------------------------------------------------------------------------------\n'
    files = [FJoin(path, file) for file in os.listdir(path)]
    countPaper = 0
    for fileInDB in files:
        loadFile = fileInDB
        vector1 = text_to_vector(fileTest)#cosinetfidf
        vector2 = text_to_vector(fileInDB)#cosinetfidf
        cosineTFIDF = get_cosine(vector1, vector2)  # Tính Cosine TF-IDF
        simR_Score = SIMR(fileTest, fileInDB)  # Tính vector thứ tự từ
        totalSIM = round((0.5 * cosineTFIDF) + (0.5 * simR_Score), 3)
        if totalSIM < newSimThreshold:
            pass
        else:
            countPaper += 1
            getPaperName = split_words_text(loadFile)[0]#tachtu
            print('split_words_text(loadFile)[0]:', split_words_text(loadFile))
            idx = cutFileName(fileInDB)  # Lấy index kí tự / cuối cùng
            print("File: ", fileInDB[idx:])  # Cắt index kí tự / cuối đến hết
            str += "File: " + fileInDB[idx:] + "\n"
            print("Tên bài báo: ", getPaperName)
            str += "Tên bài báo: " + getNamePaperEN(loadFile) + "\n"
            print("Tương đồng Cosine TF-IDF:", round(cosineTFIDF, 3))
            str += "Tương đồng Cosine TF-IDF: {0}".format(round(cosineTFIDF, 3)) + "\n"
            print("Tương đồng thứ tự từ:", round(simR_Score, 3))
            str += "Tương đồng thứ tự từ: {0}".format(round(simR_Score, 3)) + "\n"
            print("TF-IDF + SimR: {0}".format(totalSIM))
            str += "TF-IDF + SimR: {0}".format(totalSIM) + "\n"
            print('------------------------------------------------------------------------------')
            str += '------------------------------------------------------------------------------ \n'
    # End xử lý theo thư mục chỉ định
    if countPaper == 0:
        print("Không có bài báo nào tương đồng với ngưỡng !")
        str += "Không có bài báo nào tương đồng với ngưỡng !" + "\n"
    else:
        print("Số bài báo tương đồng với bài cần kiểm tra: {0}/{1}".format(countPaper, len(os.listdir(path))))
        str += "Số bài báo tương đồng với bài cần kiểm tra: {0}/{1}".format(countPaper, len(os.listdir(path))) + "\n"
    end = time.time()
    hours, rem = divmod(end - start, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Tổng thời gian xử lý: {:0>2}h:{:0>2}m:{:5.1f}s".format(int(hours), int(minutes), seconds))
    str += "Tổng thời gian xử lý: {:0>2}h:{:0>2}m:{:5.1f}s".format(int(hours), int(minutes), seconds) + "\n"
    return str

def sosanhVanBanVN(filetxt, text, nguongSS, kholinhvuc):
    str = ""
    start = time.time()
    fileTest = r''+filetxt#r"/Users/macos/Canhan/ThuChi/luanvan2/similarityProject/Kho/test/demo EN duoc dich ra VN.txt"  # r"./data/demo EN duoc dich ra VN.txt"
    # get name Paper upload
    getNameFile = fileWordTokenize(fileTest)[0]  # thay tachtu => split
    print("Tên bài báo đang được kiểm tra: ", getNameFile.replace('TÓM TẮT', ''))
    str += "Tên bài báo đang được kiểm tra: " + getNamePaperVN(fileTest) + "\n"
    # Begin Nhận vào tên thư mục và đặt giới hạn ngưỡng qua truyền tham số
    script, simThreshold = argv  # python similarityCheck.py <tenthumuc> <0.2>
    newSimThreshold = float(nguongSS)#simThreshold
    print("Ngưỡng đang kiểm tra: {0} !".format(newSimThreshold))
    str += "Ngưỡng đang kiểm tra: {0} !".format(newSimThreshold) + "\n"
    if kholinhvuc == "1":
        folderTopic = 'B-NN'#nông nghiệp
    elif kholinhvuc == "2":
        folderTopic = 'B-CN'  # Chăn nuôi, thú y topics[getIndex]
    elif kholinhvuc == "3":
        folderTopic = 'B-TS' #thủy sản
    elif kholinhvuc == "4":
        folderTopic = 'A-KHTN'
    elif kholinhvuc == "5":
        folderTopic = 'C-GD'
    else:
        folderTopic = 'B-CN'  # Chăn nuôi, thú y topics[getIndex]
    print("Chủ đề của bài cần kiểm tra là: {0}".format(folderTopic))
    str += "Chủ đề của bài cần kiểm tra là:" + folderTopic + "\n"
    if kholinhvuc == "1":
        # folderTopic = 'B-NN'#nông nghiệp
        print("THƯ MỤC ĐANG CHẠY NÔNG NGHIỆP")
        str += "NÔNG NGHIỆP" + "\n"
    elif kholinhvuc == "2":
        # folderTopic = 'B-CN'  # Chăn nuôi, thú y topics[getIndex]
        print("THƯ MỤC ĐANG CHẠY CHĂN NUÔI VÀ THÚ Y")
        str += "CHĂN NUÔI VÀ THÚ Y" + "\n"
    elif kholinhvuc == "3":
        # folderTopic = 'B-TS' #thủy sản
        print("THƯ MỤC ĐANG CHẠY THỦY SẢN")
        str += "THỦY SẢN" + "\n"
    elif kholinhvuc == "4":
        # folderTopic = 'A-KHTN'
        print("THƯ MỤC ĐANG KHOA HỌC TỰ NHIÊN")
        str += "KHOA HỌC TỰ NHIÊN" + "\n"
    elif kholinhvuc == "5":
        # folderTopic = 'C-GD'
        print("THƯ MỤC ĐANG CHẠY GIÁO DỤC")
        str += "GIÁO DỤC" + "\n"
    else:
        #folderTopic = 'B-CN'  # Chăn nuôi, thú y topics[getIndex]
        print("THƯ MỤC ĐANG CHẠY GIÁO DỤC")
        str += "GIÁO DỤC" + "\n"
    # End Dự đoán chủ đề
    # Begin xử lý theo thư mục chỉ định
    FJoin = os.path.join
    path = u''+ env + '/Kho/VIE/' + folderTopic + "/"
    print("Tổng số bài báo có trong chủ đề {0} : {1}".format(folderTopic, len(os.listdir(path))-1))
    str += "Tổng số bài báo có trong chủ đề {0} : {1}".format(folderTopic, len(os.listdir(path))-1) + "\n"
    print('------------------------------------------------------------------------------')
    str += '------------------------------------------------------------------------------\n'
    files = [FJoin(path, file) for file in os.listdir(path)]
    countPaper = 0
    for fileInDB in files:
        if fileInDB != path + ".DS_Store" :
            loadFile = fileInDB
            vector1 = text_to_vector(fileTest)#cosinetfidf
            vector2 = text_to_vector(fileInDB)#cosinetfidf
            cosineTFIDF = get_cosine(vector1, vector2)  # Tính Cosine TF-IDF
            simR_Score = SIMR(fileTest, fileInDB)  # Tính vector thứ tự từ
            totalSIM = round((0.5 * cosineTFIDF) + (0.5 * simR_Score), 3)
            if totalSIM < newSimThreshold:
                pass
            else:
                countPaper += 1
                getPaperName = fileWordTokenize(loadFile)[0]#tachtu
                idx = cutFileName(fileInDB)  # Lấy index kí tự / cuối cùng
                print("File: ", fileInDB[idx:])  # Cắt index kí tự / cuối đến hết
                str += "File: " + fileInDB[idx:] + "\n"
                print("Tên bài báo: ", getPaperName)
                str += "Tên bài báo: " + getPaperName + "\n"
                print("Tương đồng Cosine TF-IDF:", round(cosineTFIDF, 3))
                str += "Tương đồng Cosine TF-IDF: {0}".format(round(cosineTFIDF, 3)) + "\n"
                print("Tương đồng thứ tự từ:", round(simR_Score, 3))
                str += "Tương đồng thứ tự từ: {0}".format(round(simR_Score, 3)) + "\n"
                print("TF-IDF + SimR: {0}".format(totalSIM))
                str += "TF-IDF + SimR: {0}".format(totalSIM) + "\n"
                #xuất câu tương đồng
                # xử lý ngữ nghĩa
                print('file test:', fileTest)
                path_test = fileTest#"/Users/macos/Desktop/Semantic Similarity/01-CN_LUONG VINH QUOC DANH(1-7) - Copy.txt"
                file_test = open(path_test, "r+", encoding="utf-8")
                read_test = file_test.read()
                print('file train:', loadFile)
                path_train = loadFile#"/Users/macos/Desktop/Semantic Similarity/01-CN_LUONG VINH QUOC DANH(1-7).txt"
                file_train = open(path_train, "r+", encoding="utf-8")
                read_train = file_train.read()
                i = fileWordTokenize2(path_test)
                j = fileWordTokenize2(path_train)
                print("Số câu mỗi file {0} _ {1}".format(len(i), len(j)))
                str += "Số câu mỗi file {0} _ {1}".format(len(i), len(j)) + "\n"
                count = 0
                for each_senten_of_i in i:
                    for each_senten_of_j in j:
                        if check_semantic(each_senten_of_i, each_senten_of_j) == True:
                            print(each_senten_of_i)
                            str += "*** Câu tương đồng thứ {0}".format(count + 1) + ":" + "\n"
                            str += "Bản gốc: " + each_senten_of_i + "\n"
                            print(each_senten_of_j)
                            str += "Kho: " + each_senten_of_j + "\n"
                            count += 1
                            break
                            # print(count, each_senten_of_i)
                            # print(count, each_senten_of_j)

            print("Số câu tương đồng {0} / tổng {1}".format(count, len(j)))
            str += "Số câu tương đồng {0} / tổng {1}".format(count, len(j)) + "\n"
            print('------------------------------------------------------------------------------')
            str += '------------------------------------------------------------------------------ \n'
    # End xử lý theo thư mục chỉ định
    if countPaper == 0:
        print("Không có bài báo nào tương đồng với ngưỡng !")
        str += "Không có bài báo nào tương đồng với ngưỡng !" + "\n"
    else:
        print("Số bài báo tương đồng với bài cần kiểm tra: {0}/{1}".format(countPaper, len(os.listdir(path))-1))
        str += "Số bài báo tương đồng với bài cần kiểm tra: {0}/{1}".format(countPaper, len(os.listdir(path))-1) + "\n"
    end = time.time()
    hours, rem = divmod(end - start, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Tổng thời gian xử lý: {:0>2}h:{:0>2}m:{:5.1f}s".format(int(hours), int(minutes), seconds))
    str += "Tổng thời gian xử lý: {:0>2}h:{:0>2}m:{:5.1f}s".format(int(hours), int(minutes), seconds) + "\n"
    return str