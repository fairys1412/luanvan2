#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import pandas as pd
import math
import os
import sys
import re
import tachtu
import splitword
# Tạo một đối tượng regex
WORD = re.compile(r"\w+")


def get_cosine(vec1, vec2):
    # Giao vec1 và vec2 để Tập hợp các phần tử thuộc cả vec1 và vec2
    intersection = set(vec1.keys()) & set(vec2.keys())
    # Tử số (lấy số lần xuất hiện của 1 từ mà từ đó xuất hiện ở cả vec1 và vec2 nhân với nhau, lấy tổng số)
    numerator = sum([vec1[x] * vec2[x] for x in intersection])
    sum1 = sum([vec1[x] ** 2 for x in list(vec1.keys())])
    sum2 = sum([vec2[x] ** 2 for x in list(vec2.keys())])
    # Mẫu số
    denominator = math.sqrt(sum1) * math.sqrt(sum2)

    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator


def text_to_vector(filePath):
    # print(filePath)
    content = open(filePath, encoding='utf-8')
    contentFile = content.read()
    # contentFile = tachtu.fileWordTokenize(filePath)[1] # Tách từ cho tiếng Việt.
    contentFile = splitword.split_words_text(filePath) # Tách từ cho tiếng Anh.
    # print('\n', contentFile)
    # Khởi tạo đối tượng vector
    tfidfvectorizer = TfidfVectorizer(analyzer='word')
    tfidf_wm = tfidfvectorizer.fit_transform([contentFile])
    tfidf_tokens = tfidfvectorizer.get_feature_names()
    df_tfidfvect = pd.DataFrame(data=tfidf_wm.toarray(), columns=tfidf_tokens)
    dictR = df_tfidfvect.to_dict(orient='index')
    dictKeys = dictR.keys()
    dictRV = {}
    for x in dictKeys:
        dictRV = dict(dictR[x])
    return dictRV


# file1 = r'../Kho/VIE/A-KHTN/01-H8-NGUYEN CUONG QUOC(1-9)105.txt' #r"./data/Dich nguoc/01-AG-DANG MINH HIEN(1-7)088.txt"
# file1 = r"./data/File convert ENG/12-AG-PHAM THANH VU(81-88).txt"
# file1 = r"./data/L-LA/Dich nguoc/09-LA-NGUYEN THI BAO ANH(73-79)026.txt"
# file2 = r"../Kho/ENG/A-KHTN/01-NS-HUYNH NHU THAO(1-8)009.txt"
# file1 = r"data\Dich nguoc\03-AV-TRAN NGOC BICH(16-21)019.txt"
# file2 = r"data\File convert ENG\AV\03-AV-TRAN NGOC BICH(16-21)019.txt"

# fileDichNguoc = r'./data/Dich nguoc/SE-HUYNH NHUT PHUONG(148-159)060.txt'
#
# fileGoc = r"./data/File convert ENG/SE/SE-HUYNH NHUT PHUONG(148-159)060.txt"

# vector1 = text_to_vector(file1)
# vector2 = text_to_vector(file2)
# cosine = get_cosine(vector1, vector2)
# print("Tương đồng Cosine TF-IDF:", round(cosine * 100, 2))
