import regex as re


def DOIcheck(text):  # Kiểm tra mã DOI của tài liệu
    result = re.findall(r'DOI:[ ]{0,}\b(10[.][0-9]{4,}(?:[.][0-9]+)*[ ]{0,}/[ ]{0,}(?:(?!["&\'<>])\S)+)\b', text)
    if len(result) != 0:
        return True
    else:
        return False


def listToString(s):
    str1 = " "
    return (str1.join(s))


def stringToList(string):
    li = list(string.split(" "))
    return li


def fileWordTokenize(fileName):
    fileInput = open(fileName, "r+", encoding="utf-8")
    readFile = fileInput.read()

    splitFileWithLines = readFile.split("\n")  # chia file thành các dòng ứng với từng phần tử trong LIST

    for sentences in splitFileWithLines:  # Xoá các khoảng trắng thừa trong các câu
        splitFileWithLines[splitFileWithLines.index(sentences)] = " ".join(sentences.split())

    for number in range(len(splitFileWithLines)):  # Dùng để thay thế TRỪU TƯỢNG thành TÓM TẮT
        if splitFileWithLines[number] == 'TRỪU TƯỢNG':  #
            splitFileWithLines[number] = 'TÓM TẮT'

        if "..." in splitFileWithLines[number]:
            splitFileWithLines[number] = splitFileWithLines[number].replace("...", "")

    abstract_index = 0
    abstract_vn_index = 0
    start_index = 0
    end_index = 0

    for sentences in splitFileWithLines:
        if sentences == 'ABSTRACT':
            abstract_index = splitFileWithLines.index(sentences)
        if sentences == 'TÓM TẮT':
            abstract_vn_index = splitFileWithLines.index(sentences)
        if sentences == 'TÀI LIỆU THAM KHẢO':
            end_index = splitFileWithLines.index(sentences)

    if abstract_index < abstract_vn_index:
        start_index = abstract_index
    else:
        start_index = abstract_vn_index

    splitFileWithLines_after = []

    for number in range(start_index + 1, end_index):
        splitFileWithLines_after.append(splitFileWithLines[number])

    TITTLE_re = """(^\d |)[A-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỰ \-'"]{1,}"""
    tittle_re = """^\d{1,}[.]{1,}\d{1,}[.]{0,}\d{0,}[.]{0,}\d{0,}[.]{0,}\s{0,}[-a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂưăạảấầẩẫậắằẳẵặẹẻẽếềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ, 0123456789\-'"\[\]]{1,}"""
    pic_table_re = """(^Hình |^Bảng )[0-9][a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂưăạảấầẩẫậắằẳẵặẹẻẽếềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ,':/{}()!#@$%^&*_+=;?~`| 0123456789\-'"\[\]]{1,}"""
    step_re = """(^Bước )[0-9][a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂưăạảấầẩẫậắằẳẵặẹẻẽếềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ,':/{}()!#@$%^&*_+=;?~`| 0123456789\-'"\[\].]{1,}"""
    number_re = """(^\d|)+[\W\d_]{0,}"""

    splitFile = []
    for sentences in splitFileWithLines_after:  # Bỏ các tiêu đề, bảng và hình
        checktittle = re.sub(tittle_re, '', sentences)
        checkTITTLE = re.sub(TITTLE_re, '', sentences)
        checkpic_table = re.sub(pic_table_re, '', sentences)
        checkstep = re.sub(step_re, '', sentences)
        checknumber = re.sub(number_re, '', sentences)

        if len(checktittle) != 0 and len(checkTITTLE) != 0 and len(checkpic_table) != 0 and len(checkstep) != 0 and len(
                checknumber) != 0:
            splitFile.append(sentences)

    join_splitFile = listToString(splitFile)

    final_fileWordTokenize = join_splitFile.split(". ")

    return final_fileWordTokenize

# for i in fileWordTokenize("D:/OneDrive - pndd/Luận văn 2021-2022/Semantic Similarity/01-CN_LUONG VINH QUOC DANH(1-7).txt"):
#     print(i + "\n")
